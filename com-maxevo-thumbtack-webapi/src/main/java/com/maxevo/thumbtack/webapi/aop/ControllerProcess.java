package com.maxevo.thumbtack.webapi.aop;

import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.webapi.aop.base.BaseAop;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/23 11:11
 */
@ControllerAdvice
public class ControllerProcess extends BaseAop implements ResponseBodyAdvice {

    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        ResponseMessage responseMessage = null;
        CustomException customException;
        StringBuilder message;
        Map<String, Object> innerResponse;
        int status;
        String path;
        if ((path = serverHttpRequest.getURI().toString().replaceAll("^http://[^/]*", "")).matches("/error$")) {
            innerResponse = (Map<String, Object>) o;
            if (null != innerResponse.get("status")) {
                status = -1 * (Integer) innerResponse.get("status");
                path = (String) innerResponse.get("path");
                message = new StringBuilder();
                switch (status) {
                    case -400:
                        List<ObjectError> errors = (List<ObjectError>) innerResponse.get("errors");
                        List<String> defaultMessages = new ArrayList<>();
                        for (ObjectError error : errors) {
                            defaultMessages.add(error.getDefaultMessage());
                        }
                        message.append(String.join(" | ", defaultMessages));
                        break;
                    case -415:
                        message.append("Content-Type 不是要求的 | ").append(innerResponse.get("message"));
                        break;
                    default:
                        message.append(innerResponse.get("error")).append(" | ").append(innerResponse.get("message"));
                }
                customException = new CustomException(status, message.toString());
                responseMessage = new ResponseMessage(customException);
                responseMessage.setBody(o);
                LOG.warn("请求：【" + path + "】 编号：【" + getRequestSerial() + "】 -> 请求失败" + " | status : " + customException.getStatus() + " | message: " + customException.getMessage());
            }

        }
        return null == responseMessage ? o : responseMessage;
    }

}
