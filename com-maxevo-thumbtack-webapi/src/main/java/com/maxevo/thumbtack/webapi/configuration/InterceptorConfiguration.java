package com.maxevo.thumbtack.webapi.configuration;

import com.maxevo.thumbtack.webapi.interceptor.SwaggerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 11:35
 */
@Configuration
public class InterceptorConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /*注册swagger相关监听*/
        registry.addInterceptor(new SwaggerInterceptor())
                .addPathPatterns(
                        "/swagger-resources/**",
                        "/webjars/**",
                        "/v2/**",
                        "/swagger-ui.html/**",
                        "/csrf"
                );
    }
}
