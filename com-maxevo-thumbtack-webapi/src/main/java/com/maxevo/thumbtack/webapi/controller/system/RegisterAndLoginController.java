package com.maxevo.thumbtack.webapi.controller.system;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.middleware.TokenTag;
import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;
import com.maxevo.thumbtack.model.redis.Token;
import com.maxevo.thumbtack.serviceapi.system.IRegisterAndLoginService;
import com.maxevo.thumbtack.webapi.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/11 20:36
 */
@Api(value = "注册|登录", tags = {"注册|登录"})
@RestController
@RequestMapping("/api/RandL")
public class RegisterAndLoginController extends BaseController {

    @Reference
    private IRegisterAndLoginService registerAndLoginService;

    @ApiOperation(value = "前台用户注册", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/register", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<UserInfo> register(UserInfo userInfo) {
        userInfo.setRole(Role.USER);
        return makeResult(registerAndLoginService.register(userInfo));
    }

    @ApiOperation(value = "swagger 前台 登录", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/swaggerLogin", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<IRegisterAndLoginService.SwaggerLoginResponseBody> swaggerLogin(@Valid IRegisterAndLoginService.LoginRequestBody param, HttpServletResponse response) {
        Token token = TokenTag.TOKEN_SWAGGER_API.create(registerAndLoginService.login(param));
        setResponseCookie(token, response);
        LOG.info("swagger请求编号：【" + getRequestSerial() + "】 以用户身份【id：" + token.getUserInfo().getUserId() + " | userName：" + token.getUserInfo().getUserName() + "】 登录成功！");
        return makeResult(new IRegisterAndLoginService.SwaggerLoginResponseBody(token, "./"));
    }

    @ApiOperation(value = "用户 前台 登录", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/userLogin", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<IRegisterAndLoginService.LoginResponseBody> userLogin(@Valid IRegisterAndLoginService.LoginRequestBody param, HttpServletResponse response) {
        Token token = TokenTag.TOKEN_WEB_API.create(registerAndLoginService.login(param));
        setResponseCookie(token, response);
        LOG.info("请求编号：【" + getRequestSerial() + "】 以用户身份【id：" + token.getUserInfo().getUserId() + " | userName：" + token.getUserInfo().getUserName() + "】 登录成功！");
        return makeResult(new IRegisterAndLoginService.LoginResponseBody(token));
    }

}
