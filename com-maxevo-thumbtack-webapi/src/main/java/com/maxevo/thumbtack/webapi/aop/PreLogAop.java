package com.maxevo.thumbtack.webapi.aop;

import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.model.redis.Token;
import com.maxevo.thumbtack.webapi.aop.base.BaseAop;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/23 16:09
 */
@Aspect
@Component
@Order(4)
public class PreLogAop extends BaseAop {

    @Pointcut("(execution(public * com.maxevo.thumbtack.webapi.controller..*(..))) || (execution(public * com.maxevo.thumbtack.webcommon.controller..*(..)))")
    public void preLogAop() {
    }

    @Around("preLogAop()")
    public ResponseMessage apiSupervise(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Token token = getToken();
        if (null == token) {
            LOG.info("请求：【" + getURI() + "】 编号：【" + getRequestSerial() + "】 以游客身份 开始执行！");
        } else {
            LOG.info("请求：【" + getURI() + "】 编号：【" + getRequestSerial() + "】 以用户身份【id：" + token.getUserInfo().getUserId() + " | userName：" + token.getUserInfo().getUserName() + "】 开始执行！");
        }
        return (ResponseMessage) proceedingJoinPoint.proceed();
    }
}
