package com.maxevo.thumbtack.webapi.aop;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.model.middleware.TokenTag;
import com.maxevo.thumbtack.model.redis.Token;
import com.maxevo.thumbtack.webapi.aop.base.BaseAop;
import com.maxevo.thumbtack.webcommon.utils.token.WebTokenHelper;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/21 22:58
 */
@Aspect
@Component
@Order(3)
public class TokenProcessAop extends BaseAop {

    @Pointcut("(execution(public * com.maxevo.thumbtack.webapi.controller..*(..))) || (execution(public * com.maxevo.thumbtack.webcommon.controller..*(..)))")
    public void tokenAop() {
    }

    @Around("tokenAop()")
    public ResponseMessage apiSupervise(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ResponseMessage result = null;
        Token token;
        String tokenStr = WebTokenHelper.getToken(request, TokenTag.TOKEN_WEB_API);
        if (StringUtils.isNotBlank(tokenStr)) {
            token = TokenTag.TOKEN_WEB_API.get(tokenStr);
            if (null == token) {
                throw new CustomException(ExceptionCode.TOKEN_NOT_EXIST);
            } else {
                if (!tokenStr.equals(token.getToken())) {
                    throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "token 错误");
                }
                token.checkAndUpdate(tokenStr, TokenTag.TOKEN_WEB_API);
                request.setAttribute(GlobalProperties.REQUEST_ATTR_TOKEN, token);
            }
        }
        Object temp = proceedingJoinPoint.proceed();
        if (null != temp) {
            result = (ResponseMessage) temp;
        }
        return result;
    }

}
