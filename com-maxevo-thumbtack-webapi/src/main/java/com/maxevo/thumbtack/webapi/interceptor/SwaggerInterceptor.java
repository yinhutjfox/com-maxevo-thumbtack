package com.maxevo.thumbtack.webapi.interceptor;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.common.utils.json.JSONHelper;
import com.maxevo.thumbtack.common.utils.log.LogHelper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.model.middleware.TokenTag;
import com.maxevo.thumbtack.model.redis.Token;
import com.maxevo.thumbtack.webcommon.utils.token.WebTokenHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 12:04
 */
public class SwaggerInterceptor extends LogHelper implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean accessFlag = false;
        String requestSerial = request.getAttribute(GlobalProperties.REQUEST_ATTR_SERIAL).toString();
        String tokenStr;
        Token token;
        CustomException customException;
        PrintWriter printWriter = null;
        ResponseMessage result;
        try {
            if (StringUtils.isBlank(tokenStr = WebTokenHelper.getToken(request, TokenTag.TOKEN_SWAGGER_API.getTagName()))) {
                throw new CustomException(ExceptionCode.TOKEN_BLANK);
            }
            if (null == (token = TokenTag.TOKEN_SWAGGER_API.get(tokenStr))) {
                throw new CustomException(ExceptionCode.TOKEN_NOT_EXIST);
            }
            token.checkAndUpdate(tokenStr, TokenTag.TOKEN_SWAGGER_API);
            accessFlag = true;
            LOG.info("swagger：访问编号：【" + requestSerial + "】 以身份：【id：" + token.getUserInfo().getUserId() + " | userName：" + token.getUserInfo().getUserName() + "】 访问成功！");
        } catch (Exception error) {
            customException = CustomException.getCustomException(error);
            if (null == customException) {
                customException = new CustomException(ExceptionCode.OUTER_EXCEPTION, error);
            }
            result = new ResponseMessage(customException);
            LOG.error(customException);
            LOG.warn("swagger：访问编号：【" + requestSerial + "】 因：【" + result.obtainMessage() + "】 访问失败！");
            response.sendRedirect(GlobalProperties.getSWAGGER_WEBAPI_HOST_URL() + "/login.html");
            response.setCharacterEncoding("UTF-8");
            printWriter = response.getWriter();
            JSONHelper.writeValue(printWriter, result);
            printWriter.flush();
        } finally {
            if (null != printWriter) {
                printWriter.close();
            }
        }
        return accessFlag;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
