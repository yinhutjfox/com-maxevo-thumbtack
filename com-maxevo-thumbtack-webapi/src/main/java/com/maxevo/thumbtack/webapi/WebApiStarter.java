package com.maxevo.thumbtack.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/1 16:12
 */
@SpringBootApplication
@ComponentScans({
        @ComponentScan(value = "com.maxevo.thumbtack.common.configuration"),
        @ComponentScan(value = "com.maxevo.thumbtack.webcommon.configuration"),
        @ComponentScan(value = "com.maxevo.thumbtack.webcommon.listener"),
        @ComponentScan(value = "com.maxevo.thumbtack.webcommon.controller"),
        @ComponentScan(value = "com.maxevo.thumbtack.common.utils")
})
public class WebApiStarter {

    public static void main(String[] args) {
        SpringApplication.run(WebApiStarter.class, args);
    }

}
