package com.maxevo.thumbtack.webapi.aop.base;

import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/18 17:55
 */
public abstract class BaseAop extends com.maxevo.thumbtack.webcommon.aop.base.BaseAop {

    @Autowired
    protected HttpServletRequest request;

}
