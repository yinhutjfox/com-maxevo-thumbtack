package com.maxevo.thumbtack.common.utils.order;

import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/15 17:12
 */
public enum MoveOperator implements Serializable {

    /**
     * 上移
     */
    UP,

    /**
     * 下移
     */
    DOWN;

}
