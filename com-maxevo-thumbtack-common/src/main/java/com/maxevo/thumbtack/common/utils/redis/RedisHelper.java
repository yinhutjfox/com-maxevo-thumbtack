package com.maxevo.thumbtack.common.utils.redis;

import com.maxevo.thumbtack.common.utils.json.JSONHelper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Collection;
import java.util.Set;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/18 14:59
 */
public class RedisHelper {

    private static StringRedisTemplate stringRedisTemplate;

    private RedisHelper() {
    }

    public static void set(String key, Object value) {
        if (StringUtils.isBlank(key)) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "key 不能为空");
        }
        if (value instanceof String) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else {
            stringRedisTemplate.opsForValue().set(key, JSONHelper.toJsonString(value));
        }
    }

    public static <T> T get(String key, Class<T> clazz) {
        if (StringUtils.isBlank(key)) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "key 不能为空");
        }
        if (null == clazz) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "类型不能为空");
        }
        T result = null;
        String value = stringRedisTemplate.opsForValue().get(key);
        if (StringUtils.isNotBlank(value)) {
            result = JSONHelper.parseObject(value, clazz);
        }
        return result;
    }

    public static String get(String key) {
        if (StringUtils.isBlank(key)) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "key 不能为空");
        }
        return stringRedisTemplate.opsForValue().get(key);
    }

    public static void delete(String keyPattern) {
        if (StringUtils.isBlank(keyPattern)) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "key 不能为空");
        }
        if ("*".equalsIgnoreCase(keyPattern)) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "危险操作 : key 不能为 * ");
        }
        Set<String> keys = stringRedisTemplate.keys(keyPattern);
        if (CollectionUtils.isNotEmpty(keys)) {
            stringRedisTemplate.delete(keys);
        }
    }

    public static void delete(Collection<String> keys) {
        if (CollectionUtils.isNotEmpty(keys)) {
            stringRedisTemplate.delete(keys);
        }
    }

    public static Set<String> keys(String keyPattern) {
        if (StringUtils.isBlank(keyPattern)) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "key 不能为空");
        }
        if ("*".equalsIgnoreCase(keyPattern)) {
            throw new CustomException(ExceptionCode.REDIS_EXCEPTION, "危险操作 : key 不能为 * ");
        }
        return stringRedisTemplate.keys(keyPattern);
    }

}
