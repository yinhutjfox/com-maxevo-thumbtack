package com.maxevo.thumbtack.common.utils.uuid;

import java.util.UUID;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/12 16:16
 */
public class UuidHelper {

    private UuidHelper() {
    }

    public static String buildUuid() {
        return originUuid().replace("-", "").toLowerCase();
    }

    private static String originUuid() {
        return UUID.randomUUID().toString();
    }

}
