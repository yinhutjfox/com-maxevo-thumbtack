package com.maxevo.thumbtack.common.utils.md5;

import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 16:32
 */
public class Md5Helper {

    private static final String salt = "afhk@#$zlkfdj*(&";

    public static String getMD5(String data) {
        if (StringUtils.isBlank(data)) {
            throw new CustomException(ExceptionCode.INNER_EXCEPTION, "加密字段不能为空");
        }
        return DigestUtils.md5Hex(data + salt);
    }

}
