package com.maxevo.thumbtack.common.utils.messages;

import io.swagger.annotations.ApiModel;

@ApiModel("错误标识")
public enum ExceptionCode {

    /* ---------------- 内部异常 ---------------- */

    /**
     * -99999999<br/>
     * 内部异常，用于内部抛错
     */
    INNER_EXCEPTION(-99999999, "内部错误"),

    /**
     * -99999998<br/>
     * 内部异常，可抛至前端
     */
    OUTER_EXCEPTION(-99999998, "内部错误"),

    /* ---------------- 系统性异常 ---------------- */

    /**
     * -100<br/>
     * 访问身份标识异常
     */
    TOKEN_EXCEPTION(-100, "身份标识异常"),

    /**
     * -101<br/>
     * 日志记录异常
     */
    LOG_EXCEPTION(-101, "日志记录异常"),

    /**
     * -102<br/>
     * 无权访问
     */
    PRIVILEGE_EXCEPTION(-102, "无权访问"),

    /**
     * -103<br/>
     * 分页参数错误
     */
    PAGE_PARAM_EXCEPTION(-103, "分页参数错误"),

    /**
     * -104<br/>
     * 文件扩展名有误
     */
    FILE_EXTENSION_EXCEPTION(-104, "文件扩展名有误"),

    /**
     * -105<br/>
     * 不支持的文件类型
     */
    FILE_TYPE_EXCEPTION(-105, "不支持的文件类型"),

    /**
     * -106<br/>
     * 上传文件为空
     */
    FILEUPLOAD_EXCEPTION(-106, "上传文件为空"),

    /**
     * -107<br/>
     * 所有参数为空，没有什么需要更新的
     */
    UPDATE_EXCEPTION(-107, "所有参数为空，没有什么需要更新的"),

    /**
     * -108<br/>
     * 下级还有内容，请先将下级清空
     */
    DELETE_EXCEPTION(-108, "下级还有内容，请先将下级清空"),

    /**
     * -500<br/>
     * 服务器内部错误
     */
    SERVER_EXCEPTION(-500, "服务器内部错误"),

    /**
     * -109<br/>
     * redis 异常
     */
    REDIS_EXCEPTION(-109, "redis 异常"),

    /* ---------------- 非系统性异常 ---------------- */

    /**
     * -10000<br/>
     * 用户id为空
     */
    USER_INFO_ID_BLANK(-10000, "用户id为空"),

    /**
     * -10001<br/>
     * 用户不存在
     */
    USER_INFO_NOT_EXIST(-10001, "用户不存在"),

    /**
     * -10002<br/>
     * 用户参数有误
     */
    USER_INFO_PARAM(-10002, "用户参数有误"),

    /**
     * -10003<br/>
     * 用户账号已存在
     */
    USER_INFO_ACCOUNT_EXIST(-10003, "用户账号已存在"),

    /**
     * -10200<br/>
     * 门类参数异常
     */
    CATEGORY_PARAM(-10200, "门类参数异常"),

    /**
     * -10201<br/>
     * 门类名已存在
     */
    CATEGORY_NAME_EXIST(-10201, "门类名已存在"),

    /**
     * -10202<br/>
     * 门类不存在
     */
    CATEGORY_NOT_EXIST(-10202, "门类不存在"),

    /**
     * -10300<br/>
     * token 为空
     */
    TOKEN_BLANK(-10300, "token 为空"),

    /**
     * -10301<br/>
     * token 不存在
     */
    TOKEN_NOT_EXIST(-10301, "token 不存在"),

    /**
     * -10302<br/>
     * token 过期
     */
    TOKEN_EXPIRE(-10302, "token 过期"),

    /**
     * -10400<br/>
     * 目标参数异常
     */
    GOAL_PARAM(-10400, "目标参数异常"),

    /**
     * -10401<br/>
     * 目标名已存在
     */
    GOAL_NAME_EXIST(-10401, "目标名已存在"),

    /**
     * -10402<br/>
     * 目标不存在
     */
    GOAL_NOT_EXIST(-10402, "目标不存在"),

    /**
     * -10403<br/>
     * 目标排序异常
     */
    GOAL_ORDER_EXCEPTION(-10403, "目标排序异常"),

    /**
     * -10500<br/>
     * 等级参数异常
     */
    LEVEL_PARAM(-10500, "等级参数异常"),

    /**
     * -10501<br/>
     * 等级名已经存在
     */
    LEVEL_NAME_EXIST(-10501, "等级名已经存在"),

    /**
     * -10502<br/>
     * 等级不存在
     */
    LEVEL_NOT_EXIST(-10502, "等级不存在"),

    /**
     * -10503<br/>
     * 等级排序异常
     */
    LEVEL_ORDER_EXCEPTION(-10503, "等级排序异常"),

    /**
     * -10600<br/>
     * 科目参数异常
     */
    SUBJECT_PARAM(-10600, "科目参数异常"),

    /**
     * -10601<br/>
     * 科目名已存在
     */
    SUBJECT_NAME_EXIST(-10601, "科目名已存在"),

    /**
     * -10602<br/>
     * 科目不存在
     */
    SUBJECT_NOT_EXIST(-10602, "科目不存在"),

    /**
     * -10700<br/>
     * 知识点参数异常
     */
    KNOWLEDGE_PARAM(-10700, "知识点参数异常"),

    /**
     * -10701<br/>
     * 知识点名字已存在
     */
    KNOWLEDGE_NAME_EXIST(-10701, "知识点名字已存在"),

    /**
     * -10702<br/>
     * 知识点不存在
     */
    KNOWLEDGE_NOT_EXIST(-10702, "知识点不存在"),

    /**
     * -10703<br/>
     * 知识点排序异常
     */
    KNOWLEDGE_ORDER_EXCEPTION(-10703, "知识点排序异常"),

    /**
     * -10800<br/>
     * 登录参数问题
     */
    LOGING_PARAM(-10800, "登录参数问题"),

    /**
     * -10801<br/>
     * 账号密码错误
     */
    LOGING_EXCEPTION(-10801, "账号密码错误"),

    /**
     * -10900<br/>
     * 参数错误
     */
    KNOWLEDGE_CONFIG_PARAM(-10900, "参数错误"),

    /**
     * -10901<br/>
     * 该知识点在当前目标下已经配置过了
     */
    KNOWLEDGE_CONFIG_EXIST(-10901, "该知识点在当前目标下已经配置过了"),

    /**
     * -10902<br/>
     * 该知识点在当前目标下还没有配置过
     */
    KNOWLEDGE_CONFIG_NOT_EXIST(-10902, "该知识点在当前目标下还没有配置过"),

    /**
     * -10903<br/>
     * 知识点配置排序异常
     */
    KNOWLEDGE_CONFIG_ORDER_EXCEPTION(-10703, "知识点配置排序异常"),
    ;

    private final int status;
    private final String message;

    ExceptionCode(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
