package com.maxevo.thumbtack.common.utils.file;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 10:27
 */
public class FilePathBuilder {

    private String fileName;

    private FileType fileType;

    private FolderType folderType;

    private FilePathBuilder() {
    }

    public static FilePathBuilder builder(FolderType folderType, String fileName, FileType fileType) {
        FilePathBuilder builder = new FilePathBuilder();
        builder.fileName = fileName;
        builder.fileType = fileType;
        builder.folderType = folderType;
        return builder;
    }

    public String getAbsolutePath() {
        StringBuilder path = new StringBuilder();
        path.append(GlobalProperties.getFILE_COMMON_PATH()).append("/")
                .append(folderType.toString()).append("/")
                .append(fileName).append(".").append(fileType.toString());
        return path.toString();
    }

    public String getRelativePath() {
        StringBuilder path = new StringBuilder();
        path.append("/").append(folderType.toString()).append("/")
                .append(fileName).append(".").append(fileType.toString());
        return path.toString();
    }

    public static enum FolderType {

        /**
         * 门类封面路径
         */
        CATEGORY_COVER,

        ;

    }

}
