package com.maxevo.thumbtack.common.utils.messages;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2018-09-17
 */
@JsonPropertyOrder({"header , body , footer"})
public class ResponseMessage<T> implements Serializable {

    private static final long serialVersionUID = -2812100476539199065L;
    private static final String SUCCESS_MSG = "SUCCESS";
    private static final int SUCCESS_STATUS = 200;

    @ApiModelProperty(value = "响应头")
    private ResponseHeader header;

    @ApiModelProperty(value = "响应体<p style='color:yellow'>有数据才有，没有数据就没有</p>")
    private T body = null;

    @ApiModelProperty(value = "响应尾")
    private ResponseFooter footer;

    public ResponseMessage() {
        this.header = new ResponseHeader();
        this.footer = new ResponseFooter();
    }

    public ResponseMessage(T body) {
        this();
        this.body = body;
        if (null != body) {
            this.header.setHasBody(true);
        }
    }

    public ResponseMessage(CustomException customException) {
        this.header = new ResponseHeader(customException);
        this.footer = new ResponseFooter();
    }


    /**
     * Gets the value of {@link #header}.
     *
     * @return the value of {@link #header}
     */
    public ResponseHeader getHeader() {
        return header;
    }

    /**
     * Gets the value of {@link #body}.
     *
     * @return the value of {@link #body}
     */
    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
        if (null != body) {
            this.header.setHasBody(true);
        }
    }

    @JsonIgnore
    public int obtainStatus() {
        return header.getStatus();
    }

    @JsonIgnore
    public String obtainMessage() {
        return header.getMessage();
    }

    /**
     * Gets the value of {@link #footer}.
     *
     * @return the value of {@link #footer}
     */
    public ResponseFooter getFooter() {
        return footer;
    }

    public void setError(CustomException customException) {
        header.setError(customException);
    }

    private static class ResponseHeader implements Serializable {

        private static final long serialVersionUID = -370910093272563954L;

        @ApiModelProperty(value = "响应时间戳")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss:SS", timezone = "GMT+8")
        private Date timestamp;

        @ApiModelProperty(value = "响应消息<p style='color:green'>成功：SUCCESS</p><p style='color:red'>失败：相关错误信息</p>")
        private String message = SUCCESS_MSG;

        @ApiModelProperty(value = "响应状态<p style='color:green'>成功：200</p><p style='color:red'>失败：负数</p>")
        private int status = SUCCESS_STATUS;

        @ApiModelProperty(value = "返回中是否有数据<p style='color:green'>true 有数据</p><p style='color:red'>false 没有数据</p>")
        private boolean hasBody = false;


        public ResponseHeader() {
            timestamp = new Date();
        }

        public ResponseHeader(CustomException customException) {
            this();
            this.status = customException.getStatus();
            this.message = customException.getMessage();
        }

        public void setError(CustomException customException) {
            this.status = customException.getStatus();
            this.message = customException.getMessage();
        }

        /**
         * Gets the value of {@link #timestamp}.
         *
         * @return the value of {@link #timestamp}
         */
        public Date getTimestamp() {
            return timestamp;
        }

        /**
         * Gets the value of {@link #status}.
         *
         * @return the value of {@link #status}
         */
        public int getStatus() {
            return status;
        }

        /**
         * Gets the value of {@link #message}.
         *
         * @return the value of {@link #message}
         */
        public String getMessage() {
            return message;
        }

        public boolean getHasBody() {
            return hasBody;
        }

        /**
         * Sets the {@link #hasBody}.
         *
         * <p>You can use getHasBody() to get the value of {@link #hasBody}</p>
         *
         * @param hasBody hasBody
         */
        public void setHasBody(boolean hasBody) {
            this.hasBody = hasBody;
        }
    }

    private static class ResponseFooter implements Serializable {

        private static final long serialVersionUID = 5987038572376468745L;

        @ApiModelProperty(value = "版权信息")
        public String getCOPYRIGHT() {
            return GlobalProperties.getCOPYRIGHT();
        }
    }
}
