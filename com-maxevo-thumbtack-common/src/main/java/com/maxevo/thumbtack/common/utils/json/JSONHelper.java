package com.maxevo.thumbtack.common.utils.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/18 15:03
 */
public class JSONHelper {

    private static final ObjectMapper MAPPER;

    static {
        MAPPER = new ObjectMapper();
        MAPPER.configure(SerializationFeature.INDENT_OUTPUT, true);
        MAPPER.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    }

    private JSONHelper() {
    }

    public static String toJsonString(Object object) {
        StringWriter stringWriter = new StringWriter();
        try {
            MAPPER.writeValue(stringWriter, object);
        } catch (Exception error) {
            throw new CustomException(ExceptionCode.INNER_EXCEPTION, error);
        }
        return stringWriter.toString();
    }

    public static void writeValue(Writer writer, Object value) {
        try {
            MAPPER.writeValue(writer, value);
        } catch (IOException error) {
            throw new CustomException(ExceptionCode.INNER_EXCEPTION, error);
        }
    }

    public static <T> T parseObject(String jsonString, Class<T> classType) {
        T result;
        try {
            result = MAPPER.readValue(jsonString, classType);
        } catch (Exception error) {
            throw new CustomException(ExceptionCode.INNER_EXCEPTION, error);
        }
        return result;
    }

}
