package com.maxevo.thumbtack.common.utils.role;

import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/17 17:17
 */
public enum Role implements Serializable {

    /**
     * 管理员
     */
    ADMIN,

    /**
     * 用户
     */
    USER,
    ;
}
