package com.maxevo.thumbtack.common.utils.log;


import org.apache.log4j.Logger;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 15:55
 */
public abstract class LogHelper {

    protected Logger LOG = Logger.getLogger(this.getClass());

}
