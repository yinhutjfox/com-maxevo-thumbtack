package com.maxevo.thumbtack.common.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 11:18
 */
@NoArgsConstructor
@Configuration
@PropertySource(value = {
        "classpath:configuration/environment-values.properties"
}, encoding = "UTF-8")
public class GlobalProperties {

    public static final String REQUEST_ATTR_TOKEN = "token";

    public static final String REQUEST_ATTR_SERIAL = "serial";

    @Getter
    private static String COPYRIGHT;

    @Getter
    private static String WEBAPI_BASEURL;

    @Getter
    private static String WEBAPI_VERSION;

    @Getter
    private static String WEBADMIN_BASEURL;

    @Getter
    private static String WEBADMIN_VERSION;

    @Getter
    private static String SWAGGER_WEBAPI_HOST;

    @Getter
    private static String SWAGGER_WEBADMIN_HOST;

    @Getter
    private static String FILE_COMMON_PATH;

    @Getter
    private static String FILE_PATH_MATCH;

    @Getter
    private static String CATEGORY_COVER_BASE_PATH;

    @Getter
    private static String GOAL_COVER_BASE_PATH;

    @Getter
    private static String LEVEL_COVER_BASE_PATH;

    @Getter
    private static String SUBJECT_COVER_BASE_PATH;

    @Getter
    private static String KNOWLEDGE_COVER_BASE_PATH;

    @Getter
    private static String KNOWLEDGE_CONTENT_BASE_PATH;

    public static String getSWAGGER_WEBAPI_HOST_URL() {
        return "http://" + SWAGGER_WEBAPI_HOST;
    }

    public static String getSWAGGER_WEBADMIN_HOST_URL() {
        return "http://" + SWAGGER_WEBADMIN_HOST;
    }

    @Value("${custom.copyright}")
    private void setCOPYRIGHT(String COPYRIGHT) {
        GlobalProperties.COPYRIGHT = COPYRIGHT;
    }

    @Value("${custom.webapi.baseURL}")
    public void setWebapiBaseurl(String webapiBaseurl) {
        WEBAPI_BASEURL = webapiBaseurl;
    }

    @Value("${custom.webapi.version}")
    public void setWebapiVersion(String webapiVersion) {
        WEBAPI_VERSION = webapiVersion;
    }

    @Value("${custom.webadmin.baseURL}")
    public void setWebadminBaseurl(String webadminBaseurl) {
        WEBADMIN_BASEURL = webadminBaseurl;
    }

    @Value("${custom.webadmin.version}")
    public void setWebadminVersion(String webadminVersion) {
        WEBADMIN_VERSION = webadminVersion;
    }

    @Value("${swagger.webapi.host}")
    public void setSwaggerWebapiHost(String swaggerWebapiHost) {
        SWAGGER_WEBAPI_HOST = swaggerWebapiHost;
    }

    @Value("${swagger.webadmin.host}")
    public void setSwaggerWebadminHost(String swaggerWebadminHost) {
        SWAGGER_WEBADMIN_HOST = swaggerWebadminHost;
    }

    @Value("${custom.file.commonPath}")
    public void setFileCommonPath(String fileCommonPath) {
        FILE_COMMON_PATH = fileCommonPath;
    }

    @Value("${custom.file.pathMatch}")
    public void setFilePathMatch(String filePathMatch) {
        FILE_PATH_MATCH = filePathMatch;
    }

    @Value("${custom.basePath.categoryCover}")
    public void setCategoryCoverBasePath(String categoryCoverBasePath) {
        CATEGORY_COVER_BASE_PATH = categoryCoverBasePath;
    }

    @Value("${custom.basePath.goalCover}")
    public void setGoalCoverBasePath(String goalCoverBasePath) {
        GOAL_COVER_BASE_PATH = goalCoverBasePath;
    }

    @Value("${custom.basePath.levelCover}")
    public void setLevelCoverBasePath(String levelCoverBasePath) {
        LEVEL_COVER_BASE_PATH = levelCoverBasePath;
    }

    @Value("${custom.basePath.subjectCover}")
    public void setSubjectCoverBasePath(String subjectCoverBasePath) {
        SUBJECT_COVER_BASE_PATH = subjectCoverBasePath;
    }

    @Value("${custom.basePath.knowledgeCover}")
    public void setKnowledgeCoverBasePath(String knowledgeCoverBasePath) {
        KNOWLEDGE_COVER_BASE_PATH = knowledgeCoverBasePath;
    }

    @Value("${custom.basePath.knowledgeContent}")
    public void setKnowledgeContentBasePath(String knowledgeContentBasePath) {
        KNOWLEDGE_CONTENT_BASE_PATH = knowledgeContentBasePath;
    }
}
