package com.maxevo.thumbtack.common.utils.messages;

import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2018-09-17
 */
public class CustomException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 6202076010005069803L;

    private int status;
    private ExceptionCode exceptionCode;

    public CustomException() {
        super();
    }

    public CustomException(ExceptionCode exceptionCode) {
        super(exceptionCode.getMessage());
        this.status = exceptionCode.getStatus();
        this.exceptionCode = exceptionCode;
    }

    public CustomException(ExceptionCode exceptionCode, String message) {
        super(message);
        this.status = exceptionCode.getStatus();
        this.exceptionCode = exceptionCode;
    }

    public CustomException(int status, String message) {
        super(message);
        this.status = status;
    }

    public CustomException(ExceptionCode exceptionCode, Throwable throwable) {
        super(exceptionCode.getMessage() + "  由于：" + throwable.getMessage(), throwable.getCause());
        this.status = exceptionCode.getStatus();
        this.exceptionCode = exceptionCode;
    }

    public CustomException(ExceptionCode exceptionCode, String message, Throwable throwable) {
        super(message + "  由于：" + throwable.getMessage(), throwable.getCause());
        this.status = exceptionCode.getStatus();
        this.exceptionCode = exceptionCode;
    }

    public static CustomException getCustomException(Throwable throwable) {
        CustomException result = null;
        if (throwable instanceof CustomException) {
            result = (CustomException) throwable;
        } else {
            while (null != throwable.getCause()) {
                throwable = throwable.getCause();
                if (throwable instanceof CustomException) {
                    result = (CustomException) throwable;
                    break;
                }
            }
        }
        if (null != result) {
            if (ExceptionCode.INNER_EXCEPTION == result.exceptionCode) {
                result = null;
            }
        }
        return result;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public ExceptionCode getExceptionCode() {
        return exceptionCode;
    }
}
