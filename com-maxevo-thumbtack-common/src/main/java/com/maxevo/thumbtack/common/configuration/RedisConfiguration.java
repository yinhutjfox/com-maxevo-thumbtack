package com.maxevo.thumbtack.common.configuration;

import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.redis.RedisHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.lang.reflect.Field;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/18 10:21
 */
@Configuration
@PropertySource(value = "classpath:configuration/redis.properties", encoding = "UTF-8")
public class RedisConfiguration {

    @Bean(name = "jedisPoolConfig")
    public JedisPoolConfig jedisPoolConfig(
            @Value("${redis.maxIdle}") int maxIdle,
            @Value("${redis.maxTotal}") int maxTotal,
            @Value("${redis.maxWaitMillis}") int maxWaitMillis,
            @Value("${redis.minEvictableIdleTimeMillis}") int minEvictableIdleTimeMillis,
            @Value("${redis.numTestsPerEvictionRun}") int numTestsPerEvictionRun,
            @Value("${redis.timeBetweenEvictionRunsMillis}") int timeBetweenEvictionRunsMillis,
            @Value("${redis.testOnBorrow}") boolean testOnBorrow,
            @Value("${redis.testWhileIdle}") boolean testWhileIdle
    ) {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(maxIdle);
        config.setMaxTotal(maxTotal);
        config.setMaxWaitMillis(maxWaitMillis);
        config.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        config.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
        config.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        config.setTestOnBorrow(testOnBorrow);
        config.setTestWhileIdle(testWhileIdle);
        return config;
    }

    @Autowired
    @Bean(name = "jedisPool")
    public JedisPool jedisPool(
            @Qualifier("jedisPoolConfig") JedisPoolConfig config,
            @Value("${redis.hostname}") String hostname,
            @Value("${redis.port}") int port
    ) {
        return new JedisPool(config, hostname, port);
    }

    @Autowired
    public void initRedisHelper(
            StringRedisTemplate stringRedisTemplate
    ) {
        try {
            Class<?> clazz = RedisHelper.class;
            Field stringRedisTemplateField = clazz.getDeclaredField("stringRedisTemplate");
            stringRedisTemplateField.setAccessible(true);
            stringRedisTemplateField.set(null, stringRedisTemplate);
            stringRedisTemplateField.setAccessible(false);
        } catch (Exception error) {
            throw new CustomException(ExceptionCode.INNER_EXCEPTION, error);
        }
    }


}
