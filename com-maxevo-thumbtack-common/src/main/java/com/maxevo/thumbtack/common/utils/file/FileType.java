package com.maxevo.thumbtack.common.utils.file;

import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import org.apache.commons.lang3.StringUtils;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 11:30
 */
public enum FileType {

    /**
     * jpg
     */
    JPG,

    /**
     * bmp
     */
    BMP,

    /**
     * png
     */
    PNG,

    /**
     * jpeg
     */
    JPEG;

    public static FileType getByExtension(String extension) {
        FileType result = null;
        if (StringUtils.isBlank(extension)) {
            throw new CustomException(ExceptionCode.FILE_EXTENSION_EXCEPTION);
        }
        if ("jpg".equalsIgnoreCase(extension)) {
            result = JPG;
        } else if ("bmp".equalsIgnoreCase(extension)) {
            result = BMP;
        } else if ("PNG".equalsIgnoreCase(extension)) {
            result = PNG;
        } else if ("jpeg".equalsIgnoreCase(extension)) {
            result = JPEG;
        } else if ("exe|sh|bat".contains(extension.toLowerCase())) {
            throw new CustomException(ExceptionCode.FILE_TYPE_EXCEPTION, "危险的文件类型");
        } else {
            throw new CustomException(ExceptionCode.FILE_TYPE_EXCEPTION);
        }
        return result;
    }

}
