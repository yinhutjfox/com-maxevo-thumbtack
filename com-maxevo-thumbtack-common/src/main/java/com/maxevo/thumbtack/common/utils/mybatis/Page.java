package com.maxevo.thumbtack.common.utils.mybatis;

import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/6 21:49
 */
@NoArgsConstructor
public class Page implements Serializable {

    private static final long serialVersionUID = 1415831135452740352L;

    @ApiParam("第几页<p style='color:red'>从1起</p>")
    @Getter
    @Setter
    private Integer page;

    @ApiParam("有几行")
    @Getter
    @Setter
    private Integer rows;

}
