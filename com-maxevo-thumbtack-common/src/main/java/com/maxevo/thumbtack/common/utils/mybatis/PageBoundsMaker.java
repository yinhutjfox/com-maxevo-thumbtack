package com.maxevo.thumbtack.common.utils.mybatis;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;

public class PageBoundsMaker {

    public static PageBounds make() {
        return new PageBounds();
    }

    public static PageBounds make(Integer page, Integer rows) {
        PageBounds pageBounds = null;
        if (null == page || null == rows) {
            pageBounds = new PageBounds();
        } else {
            if (0 == page && 0 == rows) {
                throw new CustomException(ExceptionCode.PAGE_PARAM_EXCEPTION, "查询失败：分页参数错误，都为零");
            }
            if (0 > page || 0 > rows) {
                throw new CustomException(ExceptionCode.PAGE_PARAM_EXCEPTION, "查询失败：非法分页参数");
            }
            pageBounds = new PageBounds(page, rows);
        }
        return pageBounds;
    }

    public static PageBounds make(Page page) {
        PageBounds pageBounds;
        if (null == page) {
            pageBounds = make();
        } else {
            pageBounds = make(page.getPage(), page.getRows());
        }
        return pageBounds;
    }

}
