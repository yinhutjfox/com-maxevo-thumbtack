package com.maxevo.thumbtack.common.utils.privilege;

import com.maxevo.thumbtack.common.utils.role.Role;

import java.lang.annotation.*;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/17 17:19
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Privilege {

    Role[] accessRole() default {};

}
