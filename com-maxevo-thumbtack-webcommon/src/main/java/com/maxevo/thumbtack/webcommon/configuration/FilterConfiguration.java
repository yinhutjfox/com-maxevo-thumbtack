package com.maxevo.thumbtack.webcommon.configuration;

import com.maxevo.thumbtack.webcommon.filter.RequestSerialFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/23 17:24
 */
@Configuration
public class FilterConfiguration {

    @Bean
    public FilterRegistrationBean requestSerialFilter() {
        FilterRegistrationBean<Filter> registrationBean = new FilterRegistrationBean<>();
        Filter filter = new RequestSerialFilter();
        registrationBean.setFilter(filter);
        registrationBean.addUrlPatterns("/*");
        registrationBean.setName("requestSerialFilter");
        registrationBean.setOrder(1);
        return registrationBean;
    }

}
