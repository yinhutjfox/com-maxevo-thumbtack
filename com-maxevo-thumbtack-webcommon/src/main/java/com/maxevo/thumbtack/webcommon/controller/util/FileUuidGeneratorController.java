package com.maxevo.thumbtack.webcommon.controller.util;

import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.common.utils.uuid.UuidHelper;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/8/6 10:59
 */
@Api(value = "文件标识相关", tags = {"工具"})
@RestController
@RequestMapping("/util/FileUuidGenerator")
public class FileUuidGeneratorController extends BaseController {

    @ApiOperation(value = "根据用户获取文件标识", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/getFileUuidByUser", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<String> getFileUuidByUser() {
        return makeResult(getToken().getUserInfo().getUserId() + "-" + UuidHelper.buildUuid());
    }

}
