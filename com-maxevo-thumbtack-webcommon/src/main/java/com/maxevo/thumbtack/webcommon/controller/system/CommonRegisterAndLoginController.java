package com.maxevo.thumbtack.webcommon.controller.system;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.serviceapi.system.IRegisterAndLoginService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/11 20:36
 */
@Api(value = "注册|登录", tags = {"注册|登录"})
@RestController
@RequestMapping("/RandL")
public class CommonRegisterAndLoginController extends BaseController {

    @Reference
    private IRegisterAndLoginService registerAndLoginService;

    @ApiOperation(value = "token 登录", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/tokenLogin", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage tokenLogin() {
        if(null == getToken()) {
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "token 无效或不存在");
        }
        return null;
    }

}
