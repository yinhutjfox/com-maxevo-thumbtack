package com.maxevo.thumbtack.webcommon.controller.file;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.file.FilePathBuilder;
import com.maxevo.thumbtack.common.utils.file.FileType;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.common.utils.uuid.UuidHelper;
import com.maxevo.thumbtack.model.mybatis.mysql.FileExt;
import com.maxevo.thumbtack.serviceapi.file.IFileService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 11:51
 */
@Api(value = "文件相关", tags = {"文件相关"})
@RestController
@RequestMapping("/File")
public class FileController extends BaseController {

    @Reference
    private IFileService fileService;

    @ApiOperation(value = "文件上传", httpMethod = "POST", consumes = "multipart/*")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "fileUpload", consumes = "multipart/*")
    public ResponseMessage<FileExt> fileUpload(@RequestParam("file") MultipartFile file) {
        ResponseMessage<FileExt> result = null;
        String originFileName;
        String extension;
        File newFile;
        FileType fileType;
        FilePathBuilder filePathBuilder;
        if (null == file) {
            throw new CustomException(ExceptionCode.FILEUPLOAD_EXCEPTION);
        }
        if (StringUtils.isBlank(originFileName = file.getOriginalFilename())) {
            throw new CustomException(ExceptionCode.FILEUPLOAD_EXCEPTION, "文件名异常");
        }
        if (2 > originFileName.split("[.]").length) {
            throw new CustomException(ExceptionCode.FILEUPLOAD_EXCEPTION, "文件类型未知");
        }
        extension = originFileName.split("[.]")[1];
        fileType = FileType.getByExtension(extension);
        filePathBuilder = FilePathBuilder.builder(
                FilePathBuilder.FolderType.CATEGORY_COVER,
                UuidHelper.buildUuid(), fileType
        );
        newFile = new File(filePathBuilder.getAbsolutePath());
        try {
            file.transferTo(newFile);
            FileExt fileExt = new FileExt();
            fileExt.setFileRelativePath(filePathBuilder.getRelativePath());
            fileExt.setOriginName(originFileName);
            fileExt.setFileType(fileType);
            fileService.uploadFile(fileExt);
            result = makeResult(fileExt);
        } catch (Throwable e) {
            if (newFile.exists()) {
                newFile.delete();
            }
            if (e instanceof IOException) {
                throw new CustomException(ExceptionCode.INNER_EXCEPTION, "上传文件储存失败，请联系服务人员", e);
            } else if (null != CustomException.getCustomException(e)) {
                throw CustomException.getCustomException(e);
            } else {
                throw new CustomException(ExceptionCode.INNER_EXCEPTION, e);
            }
        }
        return result;
    }

}
