package com.maxevo.thumbtack.webcommon.controller.subject;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.mybatis.mysql.Subject;
import com.maxevo.thumbtack.model.mybatis.mysql.SubjectExt;
import com.maxevo.thumbtack.model.request.base.BaseRequestParam;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.subject.ISubjectService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 23:25
 */
@Api(value = "科目相关", tags = {"科目相关"})
@RestController
@RequestMapping("/Subject")
public class SubjectController extends BaseController {

    @Reference
    private ISubjectService subjectService;

    @ApiOperation(value = "创建科目", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/createSubject", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<SubjectExt> createSubject(@Valid SubjectExt subject) {
        return makeResult(subjectService.createSubject(subject));
    }

    @ApiOperation(value = "删除科目", httpMethod = "POST", consumes = "multipart/form-data")
    @ApiImplicitParam(name = "subjectId", value = "科目id", dataType = "String", required = true)
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/deleteSubjectById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage deleteSubjectById(String subjectId) {
        subjectService.deleteSubjectById(subjectId);
        return null;
    }

    @ApiOperation(value = "编辑科目", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/editSubjectById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage editSubjectById(@Valid ISubjectService.SubjectEdit subjectEdit) {
        subjectService.editSubjectById(subjectEdit);
        return null;
    }

    @ApiOperation(value = "获取科目", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/getSubjects", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<PageResponseBody<SubjectExt>> getSubjects(@Valid BaseRequestParam param) {
        return makeResult(subjectService.getSubjects(param));
    }

}
