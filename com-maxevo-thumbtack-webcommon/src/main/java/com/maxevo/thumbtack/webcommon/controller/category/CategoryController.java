package com.maxevo.thumbtack.webcommon.controller.category;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.mybatis.mysql.CategoryExt;
import com.maxevo.thumbtack.model.request.base.BaseRequestParam;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.category.ICategoryService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 16:31
 */
@Api(value = "门类", tags = {"门类"})
@RestController
@RequestMapping("/Category")
public class CategoryController extends BaseController {

    @Reference
    private ICategoryService categoryService;

    @ApiOperation(value = "获取门类目录", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/getCategories", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<PageResponseBody<CategoryExt>> getCategories(@Valid BaseRequestParam param) {
        return makeResult(categoryService.getCategories(param));
    }

    @ApiOperation(value = "创建门类", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/createCategory", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<CategoryExt> createCategory(@Valid CategoryExt categoryExt) {
        categoryExt.setCategoryId(categoryService.createCategory(categoryExt));
        return makeResult(categoryExt);
    }

    @ApiOperation(value = "编辑门类", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/editCategoryById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage editCategoryById(@Valid ICategoryService.CategoryEdit categoryEdit) {
        categoryService.editCategoryById(categoryEdit);
        return null;
    }

    @ApiOperation(value = "删除门类", httpMethod = "POST", consumes = "multipart/form-data")
    @ApiImplicitParam(name = "categoryId", value = "门类id", dataType = "String", required = true)
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/deleteCategoryById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage deleteCategoryById(String categoryId) {
        categoryService.deleteCategoryById(categoryId);
        return null;
    }

}
