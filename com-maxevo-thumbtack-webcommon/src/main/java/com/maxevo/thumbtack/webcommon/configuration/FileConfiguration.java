package com.maxevo.thumbtack.webcommon.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 15:07
 */
//@Configuration
public class FileConfiguration {

    @Bean("multipartResolver")
    public MultipartResolver initFileUpload() {
        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setMaxUploadSize(1073741824);
        commonsMultipartResolver.setMaxInMemorySize(1048576);
        commonsMultipartResolver.setDefaultEncoding("UTF-8");
        return commonsMultipartResolver;
    }

}
