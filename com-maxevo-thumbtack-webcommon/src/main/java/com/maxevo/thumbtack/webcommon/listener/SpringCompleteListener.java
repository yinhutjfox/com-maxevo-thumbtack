package com.maxevo.thumbtack.webcommon.listener;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.common.utils.file.FilePathBuilder;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author yinhutjfox
 * @version 1.0.0
 * @date 2019/6/17 21:29
 */
@Component
public class SpringCompleteListener implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initFolder();
    }

    void initFolder() {
        File file;
        FilePathBuilder.FolderType[] folderTypes = FilePathBuilder.FolderType.values();
        for (FilePathBuilder.FolderType folderType : folderTypes) {
            file = new File(GlobalProperties.getFILE_COMMON_PATH() + "/" + folderType.toString());
            if (!file.exists()) {
                file.mkdirs();
            }
        }
    }
}
