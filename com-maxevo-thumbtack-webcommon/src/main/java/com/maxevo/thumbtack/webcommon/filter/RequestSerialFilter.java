package com.maxevo.thumbtack.webcommon.filter;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.common.utils.uuid.UuidHelper;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/23 17:06
 */
public class RequestSerialFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setAttribute(GlobalProperties.REQUEST_ATTR_SERIAL, UuidHelper.buildUuid());
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
