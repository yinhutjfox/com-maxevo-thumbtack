package com.maxevo.thumbtack.webcommon.aop.base;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.common.utils.log.LogHelper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.redis.Token;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/22 16:16
 */
public abstract class BaseAop extends LogHelper {

    @Autowired
    protected HttpServletRequest request;

    protected Method getMethod(JoinPoint joinPoint) {
        Method[] methods = joinPoint.getTarget().getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (joinPoint.getSignature().getName().equals(method.getName())) {
                return method;
            }
        }
        throw new CustomException(ExceptionCode.INNER_EXCEPTION, "没有找到方法");
    }

    protected Map<String, Object> getParameters(JoinPoint joinPoint) {
        Map<String, Object> result = new HashMap<>();
        Object[] args = joinPoint.getArgs();
        String[] parameters = ((CodeSignature) joinPoint.getSignature()).getParameterNames();
        for (int i = 0; i < parameters.length; ++i) {
            result.put(parameters[i], args[i]);
        }
        return result;
    }

    protected Role[] checkPrivilege(Method method) {
        Role[] roles = null;
        Privilege privilege = method.getAnnotation(Privilege.class);
        if (null != privilege) {
            roles = privilege.accessRole();
        }
        return roles;
    }

    protected Token getToken() {
        Token result = null;
        Object tmp = request.getAttribute(GlobalProperties.REQUEST_ATTR_TOKEN);
        if (null != tmp) {
            result = (Token) tmp;
        }
        return result;
    }

    protected String getRequestSerial() {
        return request.getAttribute(GlobalProperties.REQUEST_ATTR_SERIAL).toString();
    }

    protected String getURI() {
        return request.getRequestURI().replaceAll("^http://[^/]*", "");
    }
}
