package com.maxevo.thumbtack.webcommon.utils.token;

import com.maxevo.thumbtack.model.middleware.TokenTag;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 13:22
 */
public class WebTokenHelper {

    public static String getToken(HttpServletRequest request, String name) {
        String result = null;
        Cookie[] cookies = request.getCookies();
        if (null != cookies && 0 != cookies.length) {
            for (Cookie cookie : cookies) {
                if (name.equalsIgnoreCase(cookie.getName())) {
                    if (StringUtils.isNotBlank(cookie.getValue())) {
                        result = cookie.getValue();
                        break;
                    }
                }
            }
        }
        if (null == result) {
            if (StringUtils.isNotBlank(request.getHeader(name))) {
                result = request.getHeader(name);
            }
        }
        return result;
    }

    public static String getToken(HttpServletRequest request, TokenTag tokenTag) {
        return getToken(request, tokenTag.getTagName());
    }

}
