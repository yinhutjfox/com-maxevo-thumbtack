package com.maxevo.thumbtack.webcommon.controller.base;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.common.utils.log.LogHelper;
import com.maxevo.thumbtack.model.redis.Token;
import com.maxevo.thumbtack.webcommon.utils.result.ResultHelper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 15:19
 */
public abstract class BaseController extends LogHelper implements ResultHelper {

    @Autowired
    protected HttpServletRequest request;

    protected String getRequestSerial() {
        return request.getAttribute(GlobalProperties.REQUEST_ATTR_SERIAL).toString();
    }

    protected String getURI() {
        return request.getRequestURI().replaceAll("^http://[^/]*", "");
    }

    protected Token getToken() {
        return (Token) request.getAttribute(GlobalProperties.REQUEST_ATTR_TOKEN);
    }

    protected void setResponseCookie(Token token, HttpServletResponse response) {
        response.setHeader("Set-Cookie", token.getTokenTag().getTagName() + "=" + token.getToken() + ";Path=/");
    }

    protected void setToken(Token token) {
        request.setAttribute(GlobalProperties.REQUEST_ATTR_TOKEN, token);
    }

}
