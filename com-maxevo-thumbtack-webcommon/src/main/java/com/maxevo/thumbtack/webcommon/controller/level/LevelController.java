package com.maxevo.thumbtack.webcommon.controller.level;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.mybatis.mysql.Level;
import com.maxevo.thumbtack.model.mybatis.mysql.LevelExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.level.ILevelService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 16:35
 */
@Api(value = "等级相关", tags = {"等级相关"})
@RestController
@RequestMapping("/Level")
public class LevelController extends BaseController {

    @Reference
    private ILevelService levelService;

    @ApiOperation(value = "创建等级", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/createLevel", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<LevelExt> createLevel(@Valid LevelExt level) {
        return makeResult(levelService.createLevel(level));
    }

    @ApiOperation(value = "根据id 删除 等级", httpMethod = "POST", consumes = "multipart/form-data")
    @ApiImplicitParam(name = "levelId", value = "要删除的等级id", dataType = "String", required = true)
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/deleteLevelById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage deleteLevelById(String levelId) {
        levelService.deleteLevelById(levelId);
        return null;
    }

    @ApiOperation(value = "根据 id 修改 等级", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/editLevelById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage editLevelById(@Valid ILevelService.LevelEdit levelEdit) {
        levelService.editLevelById(levelEdit);
        return null;
    }

    @ApiOperation(value = "根据 目标id 获取 等级", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/getLevelsByGoalId", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<PageResponseBody<LevelExt>> getLevelsByGoalId(@Valid ILevelService.GetLevelsByGoalIdRequestBody param) {
        return makeResult(levelService.getLevelsByGoalId(param));
    }

    @ApiOperation(value = "根据 id 移动 等级", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/moveLevelById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage moveLevelById(@Valid ILevelService.LevelMove levelMove) {
        levelService.moveLevelById(levelMove);
        return null;
    }
}
