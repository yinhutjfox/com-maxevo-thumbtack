package com.maxevo.thumbtack.webcommon.controller.knowledge;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.mybatis.mysql.Knowledge;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.knowledge.IKnowledgeService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/17 16:22
 */
@Api(value = "知识点相关", tags = {"知识点相关"})
@RestController
@RequestMapping("/Knowledge")
public class KnowledgeController extends BaseController {

    @Reference
    private IKnowledgeService knowledgeService;

    @ApiOperation(value = "创建知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/createKnowledge", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<KnowledgeExt> createKnowledge(@Valid KnowledgeExt knowledge) {
        return makeResult(knowledgeService.createKnowledge(knowledge));
    }

    @ApiOperation(value = "删除知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @ApiImplicitParam(name = "knowledgeId", value = "知识点id", dataType = "String", required = true)
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/deleteKnowledgeById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage deleteKnowledgeById(String knowledgeId) {
        knowledgeService.deleteKnowledgeById(knowledgeId);
        return null;
    }

    @ApiOperation(value = "修改知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/editKnowledgeById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage editKnowledgeById(@Valid IKnowledgeService.KnowledgeEdit knowledgeEdit) {
        knowledgeService.editKnowledgeById(knowledgeEdit);
        return null;
    }

    @ApiOperation(value = "获取科目下的知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/getKnowledgeBySubjectId", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<PageResponseBody<KnowledgeExt>> getKnowledgeBySubjectId(@Valid IKnowledgeService.GetKnowledgeBySubjectIsRequestBody param) {
        return makeResult(knowledgeService.getKnowledgeBySubjectId(param));
    }

    @ApiOperation(value = "移动知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/moveKnowledgeById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage moveKnowledgeById(@Valid IKnowledgeService.KnowledgeMove knowledgeMove) {
        knowledgeService.moveKnowledgeById(knowledgeMove);
        return null;
    }

}
