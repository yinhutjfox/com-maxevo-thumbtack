package com.maxevo.thumbtack.webcommon.controller.goal;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.mybatis.mysql.Goal;
import com.maxevo.thumbtack.model.mybatis.mysql.GoalExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.goal.IGoalService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/15 18:04
 */
@Api(value = "目标相关", tags = {"目标相关"})
@RestController
@RequestMapping("/Goal")
public class GoalController extends BaseController {

    @Reference
    private IGoalService goalService;

    @ApiOperation(value = "创建目标", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/createGoal", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<GoalExt> createGoal(@Valid GoalExt goalExt) {
        return makeResult(goalService.createGoal(goalExt));
    }

    @ApiOperation(value = "根据id删除目标", httpMethod = "POST", consumes = "multipart/form-data")
    @ApiImplicitParam(name = "goalId", value = "目标id", dataType = "String", required = true)
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/deleteGoalById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage deleteGoalById(String goalId) {
        goalService.deleteGoalById(goalId);
        return null;
    }

    @ApiOperation(value = "根据 id 修改目标", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/editGoalById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage editGoalById(@Valid IGoalService.GoalEdit goalEdit) {
        goalService.editGoalById(goalEdit);
        return null;
    }

    @ApiOperation(value = "根据门类 id （分页） 查看 目标列表", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/getGoalsByCategoryId", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<PageResponseBody<GoalExt>> getGoalsByCategoryId(@Valid IGoalService.GetGoalListByCategoryIdRequestBody param) {
        return makeResult(goalService.getGoalsByCategoryId(param));
    }

    @ApiOperation(value = "移动目标先后顺序", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/moveGoalById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage moveGoalById(@Valid IGoalService.GoalMove goalMove) {
        goalService.moveGoalById(goalMove);
        return null;
    }


}
