package com.maxevo.thumbtack.webcommon.utils.result;

import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 15:28
 */
public interface ResultHelper {

    default <T> ResponseMessage<T> makeResult(T body) {
        ResponseMessage<T> result = null;
        if (null != body) {
            result = new ResponseMessage<>(body);
        }
        return result;
    }

}
