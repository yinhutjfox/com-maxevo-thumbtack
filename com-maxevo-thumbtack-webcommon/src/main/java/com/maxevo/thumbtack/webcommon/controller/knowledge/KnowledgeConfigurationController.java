package com.maxevo.thumbtack.webcommon.controller.knowledge;

import com.alibaba.dubbo.config.annotation.Reference;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.privilege.Privilege;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeConfigurationExt;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.knowledge.IKnowledgeConfigurationService;
import com.maxevo.thumbtack.webcommon.controller.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/8/20 10:46
 */
@Api(value = "知识点配置相关", tags = {"知识点配置相关"})
@RestController
@RequestMapping("/KnowledgeConfig")
public class KnowledgeConfigurationController extends BaseController {

    @Reference
    private IKnowledgeConfigurationService knowledgeConfigurationService;

    @ApiOperation(value = "配置知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/configKnowledge", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage configKnowledge(@Valid IKnowledgeConfigurationService.ConfigKnowledgeRequestBody param) {
        knowledgeConfigurationService.configKnowledge(param);
        return null;
    }

    @ApiOperation(value = "从配置中移除知识点", notes = "返回移除知识点在科目中的排序", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/removeKnowledgeFromLevel", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<Integer> removeKnowledgeFromLevel(@Valid IKnowledgeConfigurationService.RemoveKnowledgeFromLevelRequestBody param) {
        return makeResult(knowledgeConfigurationService.removeKnowledgeFromLevel(param));
    }

    @ApiOperation(value = "调整知识点配置排序", httpMethod = "POST", consumes = "multipart/form-data")
    @Privilege(accessRole = Role.ADMIN)
    @PostMapping(value = "/moveKnowledgeById", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage moveKnowledgeById(@Valid IKnowledgeConfigurationService.MoveKnowledgeByIdRequestBody param) {
        knowledgeConfigurationService.moveKnowledgeById(param);
        return null;
    }

    @ApiOperation(value = "根据等级id获取配置的知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/getKnowledgeConfigurationsByLevelId", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<PageResponseBody<KnowledgeConfigurationExt>> getKnowledgeConfigurationsByLevelId(@Valid IKnowledgeConfigurationService.GetKnowledgeConfigurationsByLevelIdRequestBody param) {
        return makeResult(knowledgeConfigurationService.getKnowledgeConfigurationsByLevelId(param));
    }

    @ApiOperation(value = "根据等级id和科目id获取可用于配置的知识点", httpMethod = "POST", consumes = "multipart/form-data")
    @PostMapping(value = "/getKnowledgeCanBeUsed", consumes = {"multipart/form-data", "application/x-www-form-urlencoded"})
    public ResponseMessage<PageResponseBody<KnowledgeExt>> getKnowledgeCanBeUsed(@Valid IKnowledgeConfigurationService.GetKnowledgeCanBeUsedRequestBody param) {
        return makeResult(knowledgeConfigurationService.getKnowledgeCanBeUsed(param));
    }
}
