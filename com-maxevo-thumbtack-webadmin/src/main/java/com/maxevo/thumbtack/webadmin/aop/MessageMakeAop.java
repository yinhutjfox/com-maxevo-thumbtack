package com.maxevo.thumbtack.webadmin.aop;

import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.webadmin.aop.base.BaseAop;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/5 15:32
 */
@Aspect
@Component
@Order(2)
public class MessageMakeAop extends BaseAop {

    @Pointcut("(execution(public * com.maxevo.thumbtack.webadmin.controller..*(..))) || (execution(public * com.maxevo.thumbtack.webcommon.controller..*(..)))")
    public void messageMakeAop() {
    }

    @Around("messageMakeAop()")
    public ResponseMessage apiSupervise(ProceedingJoinPoint proceedingJoinPoint) {
        ResponseMessage result;
        try {
            Object temp = proceedingJoinPoint.proceed();
            if (null != temp) {
                result = (ResponseMessage) temp;
            } else {
                result = new ResponseMessage();
            }
        } catch (Throwable e) {
            CustomException customException = CustomException.getCustomException(e);
            if (null == customException) {
                result = new ResponseMessage(new CustomException(ExceptionCode.SERVER_EXCEPTION, e));
            } else {
                result = new ResponseMessage(customException);
            }
            LOG.error(e);
        }
        return result;
    }
}
