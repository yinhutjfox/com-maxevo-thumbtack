package com.maxevo.thumbtack.webadmin.aop;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.model.middleware.TokenTag;
import com.maxevo.thumbtack.model.redis.Token;
import com.maxevo.thumbtack.webadmin.aop.base.BaseAop;
import com.maxevo.thumbtack.webcommon.utils.token.WebTokenHelper;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/18 18:07
 */
@Aspect
@Component
@Order(3)
public class TokenProcessAop extends BaseAop {

    @Pointcut("(execution(public * com.maxevo.thumbtack.webadmin.controller..*(..))) || (execution(public * com.maxevo.thumbtack.webcommon.controller..*(..)))")
    public void tokenProcessAop() {
    }

    @Around("tokenProcessAop()")
    public ResponseMessage apiSupervise(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ResponseMessage result = null;
        Token token;
        String tokenStr = WebTokenHelper.getToken(request, TokenTag.TOKEN_WEB_ADMIN);
        if (StringUtils.isNotBlank(tokenStr)) {
            token = TokenTag.TOKEN_WEB_ADMIN.get(tokenStr);
            if (null != token) {
                token.checkAndUpdate(tokenStr, TokenTag.TOKEN_WEB_ADMIN);
                request.setAttribute(GlobalProperties.REQUEST_ATTR_TOKEN, token);
            }
        }
        Object temp = proceedingJoinPoint.proceed();
        if (null != temp) {
            result = (ResponseMessage) temp;
        }
        return result;
    }

}
