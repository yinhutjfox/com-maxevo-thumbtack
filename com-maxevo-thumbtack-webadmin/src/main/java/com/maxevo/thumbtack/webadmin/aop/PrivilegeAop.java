package com.maxevo.thumbtack.webadmin.aop;

import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.common.utils.role.Role;
import com.maxevo.thumbtack.model.redis.Token;
import com.maxevo.thumbtack.webadmin.aop.base.BaseAop;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/22 16:06
 */
@Aspect
@Component
@Order(5)
public class PrivilegeAop extends BaseAop {

    @Pointcut("(execution(public * com.maxevo.thumbtack.webadmin.controller..*(..))) || (execution(public * com.maxevo.thumbtack.webcommon.controller..*(..)))")
    public void privilegeAop() {
    }

    @Around("privilegeAop()")
    public ResponseMessage apiSupervise(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ResponseMessage result = null;
        boolean canBeAccessed = false;
        Role[] accessRole = checkPrivilege(getMethod(proceedingJoinPoint));
        Token token = getToken();
        if (null != accessRole) {
            if (null == token) {
                throw new CustomException(ExceptionCode.PRIVILEGE_EXCEPTION, "该接口需要登录才能访问");
            }
            if (0 != accessRole.length) {
                for (Role role : accessRole) {
                    if (role == token.getUserInfo().getRole()) {
                        canBeAccessed = true;
                        break;
                    }
                }
            }
            if (!canBeAccessed) {
                throw new CustomException(ExceptionCode.PRIVILEGE_EXCEPTION);
            }
        }
        Object temp;
        if (null != (temp = proceedingJoinPoint.proceed())) {
            result = (ResponseMessage) temp;
        }
        return result;
    }
}
