package com.maxevo.thumbtack.webadmin.configuration;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/3 16:21
 */
@Configuration
@EnableDubbo
@PropertySource(value = "classpath:configuration/dubbo.properties", encoding = "UTF-8")
public class DubboConfiguration {

}
