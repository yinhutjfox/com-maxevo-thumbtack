package com.maxevo.thumbtack.webadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/1 17:06
 */
@SpringBootApplication
@ComponentScans({
        @ComponentScan(value = "com.maxevo.thumbtack.common.configuration"),
        @ComponentScan(value = "com.maxevo.thumbtack.webcommon.configuration"),
        @ComponentScan(value = "com.maxevo.thumbtack.webcommon.listener"),
        @ComponentScan(value = "com.maxevo.thumbtack.webcommon.controller"),
        @ComponentScan(value = "com.maxevo.thumbtack.common.utils")
})
public class WebAdminStarter {

    public static void main(String[] args) {
        SpringApplication.run(WebAdminStarter.class, args);
    }

}
