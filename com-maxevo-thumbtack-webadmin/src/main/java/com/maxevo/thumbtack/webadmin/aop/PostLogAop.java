package com.maxevo.thumbtack.webadmin.aop;

import com.maxevo.thumbtack.common.utils.messages.ResponseMessage;
import com.maxevo.thumbtack.webadmin.aop.base.BaseAop;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/23 16:21
 */
@Aspect
@Component
@Order(1)
public class PostLogAop extends BaseAop {

    @Pointcut("(execution(public * com.maxevo.thumbtack.webadmin.controller..*(..))) || (execution(public * com.maxevo.thumbtack.webcommon.controller..*(..)))")
    public void postLogAop() {
    }

    @Around("postLogAop()")
    public ResponseMessage apiSupervise(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ResponseMessage responseMessage;
        long start = System.currentTimeMillis();
        responseMessage = (ResponseMessage) proceedingJoinPoint.proceed();
        long end = System.currentTimeMillis();
        if (200 == responseMessage.obtainStatus()) {
            LOG.info("请求：【" + getURI() + "】 编号：【" + getRequestSerial() + "】 执行成功！耗时：【" + (end - start) + "毫秒】");
        } else {
            LOG.warn("请求：【" + getURI() + "】 编号：【" + getRequestSerial() + "】 因：【" + responseMessage.obtainMessage() + "】 执行失败！耗时：【" + (end - start) + "毫秒】");
        }
        return responseMessage;
    }
}
