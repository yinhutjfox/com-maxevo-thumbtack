package com.maxevo.thumbtack.webadmin.configuration;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import com.maxevo.thumbtack.model.middleware.TokenTag;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/3 15:56
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    private static List<Parameter> parameters = new ArrayList<>();

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("thumbtack webadmin API")
                .description("&nbsp;")
                .termsOfServiceUrl("")
                .version(GlobalProperties.getWEBADMIN_VERSION())
                .contact(new Contact("JingFeng.Tan", "", "493386452@qq.com"))
                .build();
    }

    void setTokenNeed() {
        parameters.add(
                new ParameterBuilder()
                        .name(TokenTag.TOKEN_WEB_ADMIN.getTagName())
                        .description("通关令牌")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                        .required(false)
                        .build()
        );
    }

    @Bean
    public Docket customImplementation() {
        setTokenNeed();
        return new Docket(DocumentationType.SWAGGER_2)
                .host(GlobalProperties.getSWAGGER_WEBADMIN_HOST())
                .groupName("webapi 接口")
                .select()
                .paths(PathSelectors.ant("/**"))
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .build()
                .apiInfo(apiInfo())
                .globalOperationParameters(parameters);
    }

}
