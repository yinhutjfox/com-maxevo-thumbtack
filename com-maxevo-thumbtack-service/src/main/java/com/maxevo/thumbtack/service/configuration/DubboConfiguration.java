package com.maxevo.thumbtack.service.configuration;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/3 13:57
 */
@Configuration
@EnableDubbo
@DubboComponentScan(basePackages = "com.maxevo.thumbtack.service.serviceImpl")
@PropertySource(value = "classpath:/configuration/dubbo.properties", encoding = "UTF-8")
public class DubboConfiguration {
}
