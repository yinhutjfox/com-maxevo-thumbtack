package com.maxevo.thumbtack.service;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/2 16:59
 */
@SpringBootApplication
@ComponentScans({
        @ComponentScan(value = "com.maxevo.thumbtack.common.configuration"),
        @ComponentScan(value = "com.maxevo.thumbtack.common.utils")
})
public class ServiceStarter {

    private static final Logger LOGGER = Logger.getLogger(ServiceStarter.class);

    public static void main(String[] args) {
        SpringApplication.run(ServiceStarter.class, args);
        LOGGER.info("@@@@@@@@@@@@@@@@@@service启动成功@@@@@@@@@@@@@@@@@@@@@");
        while (true) {
            synchronized (ServiceStarter.class) {
                try {
                    ServiceStarter.class.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
