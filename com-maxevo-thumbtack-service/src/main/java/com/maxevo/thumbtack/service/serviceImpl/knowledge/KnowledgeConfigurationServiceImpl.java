package com.maxevo.thumbtack.service.serviceImpl.knowledge;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.PageBoundsMaker;
import com.maxevo.thumbtack.dao.mybatis.mysql.KnowledgeConfigurationMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.KnowledgeMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.LevelMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.SubjectMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.*;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.knowledge.IKnowledgeConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/8/20 10:42
 */
@Service
public class KnowledgeConfigurationServiceImpl implements IKnowledgeConfigurationService {

    @Autowired
    private KnowledgeMapper knowledgeMapper;

    @Autowired
    private LevelMapper levelMapper;

    @Autowired
    private SubjectMapper subjectMapper;

    @Autowired
    private KnowledgeConfigurationMapper knowledgeConfigurationMapper;

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public int configKnowledge(ConfigKnowledgeRequestBody param) {
        KnowledgeConfiguration knowledgeConfiguration;
        Knowledge knowledge;
        Level level;
        if(StringUtils.isBlank(param.getKnowledgeId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "知识点id不能为空");
        }
        if(StringUtils.isBlank(param.getLevelId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "等级id不能为空");
        }
        knowledge = knowledgeMapper.selectById(param.getKnowledgeId());
        level = levelMapper.selectById(param.getLevelId());
        if(null != knowledgeConfigurationMapper.selectById(knowledge.getKnowledgeId() + level.getGoalId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_EXIST);
        }
        knowledgeConfigurationMapper.insert(knowledgeConfiguration = new KnowledgeConfiguration(
                knowledge, level,
                knowledgeConfigurationMapper.selectCount(new QueryWrapper<KnowledgeConfiguration>().eq("level_id", param.getLevelId()))
        ));
        return knowledgeConfiguration.getLearnOrder();
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public int removeKnowledgeFromLevel(RemoveKnowledgeFromLevelRequestBody param) {
        KnowledgeConfiguration knowledgeConfiguration;
        if(StringUtils.isBlank(param.getKnowledgeId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "知识点id不能为空");
        }
        if(StringUtils.isBlank(param.getLevelId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "等级id不能为空");
        }
        if(null == (knowledgeConfiguration = knowledgeConfigurationMapper.selectOne(
                new QueryWrapper<KnowledgeConfiguration>()
                        .eq("knowledge_to_level_id", param.getKnowledgeId() + param.getLevelId())))) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_NOT_EXIST);
        }
        if(1 != knowledgeConfigurationMapper.delete(new QueryWrapper<KnowledgeConfiguration>()
                .eq("knowledge_to_level_id", param.getKnowledgeId() + param.getLevelId()))) {
            throw new CustomException(ExceptionCode.OUTER_EXCEPTION, "删除错误，原因未知");
        }
        knowledgeConfigurationMapper.updateOrderAfterRemove(knowledgeConfiguration.getLevelId(), knowledgeConfiguration.getLearnOrder());
        return knowledgeMapper.selectById(param.getKnowledgeId()).getKnowledgeOrder();
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void moveKnowledgeById(MoveKnowledgeByIdRequestBody param) {
        KnowledgeConfiguration target;
        KnowledgeConfiguration up, down;
        if(StringUtils.isBlank(param.getLevelId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "等级id不能为空");
        }
        if(StringUtils.isBlank(param.getKnowledgeId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "知识点id不能为空");
        }
        if(null == (target = knowledgeConfigurationMapper.selectOne(
                new QueryWrapper<KnowledgeConfiguration>()
                        .eq("knowledge_to_level_id", param.getKnowledgeId() + param.getLevelId())))) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_NOT_EXIST);
        }
        if (null == param.getMoveOperator()) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "移动类型参数为空");
        }
        up = new KnowledgeConfiguration();
        down = new KnowledgeConfiguration();
        switch (param.getMoveOperator()) {
            case UP:
                if(0 == target.getLearnOrder()) {
                    throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_ORDER_EXCEPTION, "已经是第一个啦");
                }
                up.setKnowledgeToGoalId(target.getKnowledgeToGoalId());
                up.setLearnOrder(target.getLearnOrder() - 1);
                down.setLearnOrder(target.getLearnOrder());
                knowledgeConfigurationMapper.update(down, new UpdateWrapper<KnowledgeConfiguration>()
                        .eq("level_id", target.getLevelId())
                        .eq("learn_order", up.getLearnOrder())
                );
                knowledgeConfigurationMapper.updateById(up);
                break;
            case DOWN:
                if(knowledgeConfigurationMapper.selectCount(
                        new QueryWrapper<KnowledgeConfiguration>().eq("level_id", param.getLevelId())
                ) == target.getLearnOrder() + 1) {
                    throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_ORDER_EXCEPTION, "已经是最后一个啦");
                }
                down.setKnowledgeToGoalId(target.getKnowledgeToGoalId());
                up.setLearnOrder(target.getLearnOrder());
                down.setLearnOrder(target.getLearnOrder() + 1);
                knowledgeConfigurationMapper.update(up, new UpdateWrapper<KnowledgeConfiguration>()
                        .eq("level_id", target.getLevelId())
                        .eq("learn_order", down.getLearnOrder())
                );
                knowledgeConfigurationMapper.updateById(down);
                break;
            default:
                throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "移动类型参数为空");
        }
    }

    @Override
    public PageResponseBody<KnowledgeConfigurationExt> getKnowledgeConfigurationsByLevelId(GetKnowledgeConfigurationsByLevelIdRequestBody param) {
        int total;
        PageResponseBody<KnowledgeConfigurationExt> result = null;
        if (StringUtils.isBlank(param.getLevelId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "等级id不能为空");
        }
        if(null == levelMapper.selectById(param.getLevelId())) {
            throw new CustomException(ExceptionCode.LEVEL_NOT_EXIST);
        }
        if(0 != (total = knowledgeConfigurationMapper.selectCount(new QueryWrapper<KnowledgeConfiguration>().eq("level_id", param.getLevelId())))) {
            result = new PageResponseBody<>(
                    knowledgeConfigurationMapper.getKnowledgeConfigurationsByLevelId(param.getLevelId(), PageBoundsMaker.make(param)),
                    param,
                    total
            );
        }
        return result;
    }

    @Override
    public PageResponseBody<KnowledgeExt> getKnowledgeCanBeUsed(GetKnowledgeCanBeUsedRequestBody param) {
        int total;
        Level level;
        String goalId;
        PageResponseBody<KnowledgeExt> result = null;
        if(StringUtils.isBlank(param.getLevelId()) || StringUtils.isBlank(param.getSubjectId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_CONFIG_PARAM, "等级id与科目id都不能为空");
        }
        if(null == (level = levelMapper.selectById(param.getLevelId()))) {
            throw new CustomException(ExceptionCode.LEVEL_NOT_EXIST);
        }
        if(null == subjectMapper.selectById(param.getSubjectId())) {
            throw new CustomException(ExceptionCode.SUBJECT_NOT_EXIST);
        }
        goalId = level.getGoalId();
        if(0 != (total = knowledgeMapper.getKnowledgeCanBeUsedCount(goalId, param.getSubjectId()))) {
            result = new PageResponseBody<>(
                    knowledgeMapper.getKnowledgeCanBeUsed(goalId, param.getSubjectId(), PageBoundsMaker.make(param)),
                    param,
                    total
            );
        }
        return result;
    }
}
