package com.maxevo.thumbtack.service.serviceImpl.file;

import com.alibaba.dubbo.config.annotation.Service;
import com.maxevo.thumbtack.dao.mybatis.mysql.FileMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.File;
import com.maxevo.thumbtack.serviceapi.file.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 14:25
 */
@Service
public class FileServiceImpl implements IFileService {

    @Autowired
    private FileMapper fileMapper;

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void uploadFile(File file) {
        fileMapper.insert(file);
    }
}
