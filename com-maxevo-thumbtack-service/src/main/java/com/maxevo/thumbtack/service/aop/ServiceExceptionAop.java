package com.maxevo.thumbtack.service.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/11 9:36
 */
@Aspect
@Component
public class ServiceExceptionAop {


    @Pointcut("execution(public * com.maxevo.thumbtack.service.serviceImpl..*(..))")
    public void serviceException() {
    }

    @Around("serviceException()")
    public Object exceptionProcess(ProceedingJoinPoint proceedingJoinPoint) {
        Object result = null;
        try {
            result = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        return result;
    }
}
