package com.maxevo.thumbtack.service.serviceImpl.knowledge;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.PageBoundsMaker;
import com.maxevo.thumbtack.dao.mybatis.mysql.KnowledgeConfigurationMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.KnowledgeMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.SubjectMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.Knowledge;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeConfiguration;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.knowledge.IKnowledgeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/17 12:21
 */
@Service
public class KnowledgeServiceImpl implements IKnowledgeService {

    @Autowired
    private KnowledgeMapper knowledgeMapper;

    @Autowired
    private SubjectMapper subjectMapper;

    @Autowired
    private KnowledgeConfigurationMapper knowledgeConfigurationMapper;

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public KnowledgeExt createKnowledge(KnowledgeExt knowledge) {
        if (StringUtils.isBlank(knowledge.getKnowledgeName())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "知识点 名字 为空");
        }
        if (StringUtils.isBlank(knowledge.getSubjectId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "知识点所属科目id为空");
        }
        if (null == subjectMapper.selectById(knowledge.getSubjectId())) {
            throw new CustomException(ExceptionCode.SUBJECT_NOT_EXIST);
        }
        if (null != knowledgeMapper.selectOne(new QueryWrapper<Knowledge>()
                .eq("subject_id", knowledge.getSubjectId())
                .eq("knowledge_name", knowledge.getKnowledgeName())
        )) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_NAME_EXIST);
        }
        knowledge.setKnowledgeOrder(knowledgeMapper.selectCount(new QueryWrapper<Knowledge>().eq("subject_id", knowledge.getSubjectId())));
        knowledgeMapper.insert(knowledge);
        return knowledge;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void deleteKnowledgeById(String knowledgeId) {
        Knowledge targetKnowledge = checkCanBeDeleteById(knowledgeId);
        knowledgeMapper.deleteById(knowledgeId);
        knowledgeMapper.updateOrderAfterDeleteById(targetKnowledge.getSubjectId(), targetKnowledge.getKnowledgeOrder());
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void editKnowledgeById(KnowledgeEdit knowledgeEdit) {
        Knowledge targetKnowledge;
        if (StringUtils.isBlank(knowledgeEdit.getKnowledgeId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "知识点 id 为空");
        }
        if (null == (targetKnowledge = knowledgeMapper.selectById(knowledgeEdit.getKnowledgeId()))) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_NOT_EXIST);
        }
        if (StringUtils.isNotBlank(knowledgeEdit.getKnowledgeName())
                &&
                null != knowledgeMapper.selectOne(new QueryWrapper<Knowledge>()
                        .eq("subject_id", targetKnowledge.getSubjectId())
                        .eq("knowledge_name", knowledgeEdit.getKnowledgeName())
                        .ne("knowledge_id", knowledgeEdit.getKnowledgeId())
                )
        ) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_NAME_EXIST);
        }
        knowledgeMapper.updateById(knowledgeEdit.buildKnowledge());
    }

    @Override
    public void moveKnowledgeById(KnowledgeMove knowledgeMove) {
        Knowledge targetKnowledge;
        Knowledge upKnowledge;
        Knowledge downKnowledge;
        int total;
        if (StringUtils.isBlank(knowledgeMove.getKnowledgeId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "知识点id为空");
        }
        if (null == (targetKnowledge = knowledgeMapper.selectById(knowledgeMove.getKnowledgeId()))) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_NOT_EXIST);
        }
        if (null == knowledgeMove.getMoveOperator()) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "移动参数为空");
        }
        total = knowledgeMapper.selectCount(new QueryWrapper<Knowledge>().eq("subject_id", targetKnowledge.getSubjectId()));
        upKnowledge = new Knowledge(false);
        downKnowledge = new Knowledge(false);
        switch (knowledgeMove.getMoveOperator()) {
            case UP:
                if (0 == targetKnowledge.getKnowledgeOrder()) {
                    throw new CustomException(ExceptionCode.KNOWLEDGE_ORDER_EXCEPTION, "已经是第一个啦");
                }
                upKnowledge.setKnowledgeId(knowledgeMove.getKnowledgeId());
                upKnowledge.setKnowledgeOrder(targetKnowledge.getKnowledgeOrder() - 1);
                downKnowledge.setKnowledgeOrder(targetKnowledge.getKnowledgeOrder());
                knowledgeMapper.update(downKnowledge, new QueryWrapper<Knowledge>()
                        .eq("subject_id", targetKnowledge.getSubjectId())
                        .eq("knowledge_order", targetKnowledge.getKnowledgeOrder() - 1)
                );
                knowledgeMapper.updateById(upKnowledge);
                break;
            case DOWN:
                if (total == targetKnowledge.getKnowledgeOrder() + 1) {
                    throw new CustomException(ExceptionCode.KNOWLEDGE_ORDER_EXCEPTION, "已经是最后一个啦");
                }
                downKnowledge.setKnowledgeId(knowledgeMove.getKnowledgeId());
                downKnowledge.setKnowledgeOrder(targetKnowledge.getKnowledgeOrder() + 1);
                upKnowledge.setKnowledgeOrder(targetKnowledge.getKnowledgeOrder());
                knowledgeMapper.update(upKnowledge, new QueryWrapper<Knowledge>()
                        .eq("subject_id", targetKnowledge.getSubjectId())
                        .eq("knowledge_order", targetKnowledge.getKnowledgeOrder() + 1)
                );
                knowledgeMapper.updateById(downKnowledge);
                break;
            default:
                throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "移动参数类型无效");
        }
    }

    @Override
    public PageResponseBody<KnowledgeExt> getKnowledgeBySubjectId(GetKnowledgeBySubjectIsRequestBody param) {
        PageResponseBody<KnowledgeExt> result = null;
        int total;
        if (StringUtils.isBlank(param.getSubjectId())) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "科目 id 为空");
        }
        if (null == subjectMapper.selectById(param.getSubjectId())) {
            throw new CustomException(ExceptionCode.SUBJECT_NOT_EXIST);
        }
        if (0 != (total = knowledgeMapper.selectCount(new QueryWrapper<Knowledge>().eq("subject_id", param.getSubjectId())))) {
            result = new PageResponseBody<>(
                    knowledgeMapper.getKnowledgeBySubjectId(param.getSubjectId(), PageBoundsMaker.make(param)),
                    param,
                    total
            );
        }
        return result;
    }

    private Knowledge checkCanBeDeleteById(String knowledgeId) {
        Knowledge result;
        if (StringUtils.isBlank(knowledgeId)) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_PARAM, "知识点 id 为空");
        }
        if (null == (result = knowledgeMapper.selectById(knowledgeId))) {
            throw new CustomException(ExceptionCode.KNOWLEDGE_NOT_EXIST);
        }
        if(0 != knowledgeConfigurationMapper.selectList(new QueryWrapper<KnowledgeConfiguration>().eq("knowledge_id", knowledgeId)).size()) {
            throw new CustomException(ExceptionCode.DELETE_EXCEPTION);
        }
        return result;
    }
}
