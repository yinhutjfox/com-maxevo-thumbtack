package com.maxevo.thumbtack.service.serviceImpl.subject;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.Page;
import com.maxevo.thumbtack.common.utils.mybatis.PageBoundsMaker;
import com.maxevo.thumbtack.dao.mybatis.mysql.KnowledgeMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.SubjectMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.Knowledge;
import com.maxevo.thumbtack.model.mybatis.mysql.Subject;
import com.maxevo.thumbtack.model.mybatis.mysql.SubjectExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.subject.ISubjectService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 21:11
 */
@Service
public class SubjectServiceImpl implements ISubjectService {

    @Autowired
    private SubjectMapper subjectMapper;

    @Autowired
    private KnowledgeMapper knowledgeMapper;

    @Override
    public SubjectExt createSubject(SubjectExt subject) {
        if (StringUtils.isBlank(subject.getSubjectName())) {
            throw new CustomException(ExceptionCode.SUBJECT_PARAM, "科目 名 为空");
        }
        if (null != subjectMapper.selectOne(new QueryWrapper<Subject>().eq("subject_name", subject.getSubjectName()))) {
            throw new CustomException(ExceptionCode.SUBJECT_NAME_EXIST);
        }
        subjectMapper.insert(subject);
        return subject;
    }

    @Override
    public void deleteSubjectById(String subjectId) {
        checkCanBeDeleteById(subjectId);
        subjectMapper.deleteById(subjectId);
    }

    @Override
    public void editSubjectById(SubjectEdit subjectEdit) {
        if (StringUtils.isBlank(subjectEdit.getSubjectId())) {
            throw new CustomException(ExceptionCode.SUBJECT_PARAM, "科目 id 为空");
        }
        if (null == subjectMapper.selectById(subjectEdit.getSubjectId())) {
            throw new CustomException(ExceptionCode.SUBJECT_NOT_EXIST);
        }
        if (StringUtils.isNotBlank(subjectEdit.getSubjectName())
                &&
                null != subjectMapper.selectOne(new QueryWrapper<Subject>()
                        .eq("subject_name", subjectEdit.getSubjectName())
                        .ne("subject_id", subjectEdit.getSubjectId())
                )
        ) {
            throw new CustomException(ExceptionCode.SUBJECT_NAME_EXIST);
        }
        subjectMapper.updateById(subjectEdit.buildSubject());
    }

    @Override
    public PageResponseBody<SubjectExt> getSubjects(Page page) {
        PageResponseBody<SubjectExt> result = null;
        int total;
        if (0 != (total = subjectMapper.selectCount(null))) {
            result = new PageResponseBody<>(
                    subjectMapper.getSubjects(PageBoundsMaker.make(page)), page, total
            );
        }
        return result;
    }

    private void checkCanBeDeleteById(String subjectId) {
        if (StringUtils.isBlank(subjectId)) {
            throw new CustomException(ExceptionCode.SUBJECT_PARAM, "科目 id 为空");
        }
        if (null == subjectMapper.selectById(subjectId)) {
            throw new CustomException(ExceptionCode.SUBJECT_NOT_EXIST);
        }
        if (CollectionUtils.isNotEmpty(knowledgeMapper.selectList(new QueryWrapper<Knowledge>().eq("subject_id", subjectId)))) {
            throw new CustomException(ExceptionCode.DELETE_EXCEPTION);
        }
    }
}
