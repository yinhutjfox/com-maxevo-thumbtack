package com.maxevo.thumbtack.service.serviceImpl.user;

import com.alibaba.dubbo.config.annotation.Service;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.dao.mybatis.mysql.UserInfoMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;
import com.maxevo.thumbtack.serviceapi.user.IUserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/8 0:58
 */
@Service
public class UserInforImpl implements IUserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo getUserInfoById(String userId) {
        return null;
    }

    private UserInfo checkUserId(String userId) {
        UserInfo userInfo = null;
        if (StringUtils.isBlank(userId)) {
            throw new CustomException(ExceptionCode.USER_INFO_ID_BLANK);
        }
        if (null == (userInfo = userInfoMapper.selectById(userId))) {
            throw new CustomException(ExceptionCode.USER_INFO_NOT_EXIST);
        }
        return userInfo;
    }
}
