package com.maxevo.thumbtack.service.serviceImpl.category;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.Page;
import com.maxevo.thumbtack.common.utils.mybatis.PageBoundsMaker;
import com.maxevo.thumbtack.dao.mybatis.mysql.CategoryMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.GoalMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.Category;
import com.maxevo.thumbtack.model.mybatis.mysql.CategoryExt;
import com.maxevo.thumbtack.model.mybatis.mysql.Goal;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.category.ICategoryService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/9 12:16
 */
@Service
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private GoalMapper goalMapper;

    @Override
    public PageResponseBody<CategoryExt> getCategories(Page page) {
        PageResponseBody<CategoryExt> result = null;
        int total = categoryMapper.selectCount(null);
        if (0 != total) {
            result = new PageResponseBody<>(categoryMapper.getCategories(PageBoundsMaker.make(page)), page, total);
        }
        return result;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public String createCategory(Category category) {
        if (null == category) {
            throw new CustomException(ExceptionCode.CATEGORY_PARAM);
        }
        if (StringUtils.isBlank(category.getCategoryName())) {
            throw new CustomException(ExceptionCode.CATEGORY_PARAM, "门类名为空");
        }
        if (null != categoryMapper.selectOne(new QueryWrapper<Category>().eq("category_name", category.getCategoryName()))) {
            throw new CustomException(ExceptionCode.CATEGORY_NAME_EXIST);
        }
        categoryMapper.insert(category);
        return category.getCategoryId();
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void editCategoryById(CategoryEdit categoryEdit) {
        if (StringUtils.isBlank(categoryEdit.getCategoryId())) {
            throw new CustomException(ExceptionCode.CATEGORY_PARAM, "门类id为空");
        }
        if (null == categoryMapper.selectById(categoryEdit.getCategoryId())) {
            throw new CustomException(ExceptionCode.CATEGORY_NOT_EXIST, "对应 id 门类不存在");
        }
        if (StringUtils.isNotBlank(categoryEdit.getNewCategoryName())) {
            if (null != categoryMapper.selectOne(new QueryWrapper<Category>().eq("category_name", categoryEdit.getNewCategoryName()).ne("category_id", categoryEdit.getCategoryId()))) {
                throw new CustomException(ExceptionCode.CATEGORY_NAME_EXIST);
            }
        }
        categoryMapper.updateById(categoryEdit.buildCategory());
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void deleteCategoryById(String categoryId) {
        checkCanBeDeletedById(categoryId);
        categoryMapper.deleteById(categoryId);
    }

    /**
     * 检查该 id 门类是否可以删除
     *
     * @param categoryId 要删除的门类 id
     */
    private void checkCanBeDeletedById(String categoryId) {
        if (StringUtils.isBlank(categoryId)) {
            throw new CustomException(ExceptionCode.CATEGORY_PARAM, "门类id为空");
        }
        if (null == categoryMapper.selectById(categoryId)) {
            throw new CustomException(ExceptionCode.CATEGORY_NOT_EXIST);
        }
        if (CollectionUtils.isNotEmpty(goalMapper.selectList(new QueryWrapper<Goal>().eq("category_id", categoryId)))) {
            throw new CustomException(ExceptionCode.DELETE_EXCEPTION);
        }
    }
}
