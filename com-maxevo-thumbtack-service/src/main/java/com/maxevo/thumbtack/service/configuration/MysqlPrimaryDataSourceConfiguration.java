package com.maxevo.thumbtack.service.configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.github.miemiedev.mybatis.paginator.OffsetLimitInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;


/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/6 18:36
 */
@Configuration
@MapperScans({
        @MapperScan("com.maxevo.thumbtack.dao.mybatis.mysql")
})
@PropertySource(value = "classpath:configuration/datasource.properties", encoding = "UTF-8")
@EnableTransactionManagement(proxyTargetClass = true)
public class MysqlPrimaryDataSourceConfiguration {

    @Value("${master.datasource.driverClass}")
    private String masterDatasourceDriverClass;

    @Value("${master.datasource.url}")
    private String masterDatasourceUrl;

    @Value("${master.datasource.userName}")
    private String masterDatasourceUserName;

    @Value("${master.datasource.password}")
    private String masterDatasourcePassword;

    @Value("${master.datasource.initialSize}")
    private int masterDatasourceInitialSize;

    @Value("${master.datasource.minIdle}")
    private int masterDatasourceMinIdle;

    @Value("${master.datasource.maxActive}")
    private int masterDatasourceMaxActive;

    @Value("${master.datasource.maxWait}")
    private int masterDatasourceMaxWait;

    @Value("${master.datasource.useUnfairLock}")
    private boolean masterDatasourceUseUnfairLock;

    @Value("${master.datasource.timeBetweenEvictionRunsMillis}")
    private int masterDatasourceTimeBetweenEvictionRunsMillis;

    @Value("${master.datasource.minEvictableIdleTimeMillis}")
    private int masterDatasourceMinEvictableIdleTimeMillis;

    @Value("${master.datasource.validationQuery}")
    private String masterDatasourceValidationQuery;

    @Value("${master.datasource.testWhileIdle}")
    private boolean masterDatasourceTestWhileIdle;

    @Value("${master.datasource.testOnBorrow}")
    private boolean masterDatasourceTestOnBrowse;

    @Value("${master.datasource.testOnReturn}")
    private boolean masterDatasourceTestOnReturn;

    @Value("${master.datasource.poolPreparedStatement}")
    private boolean masterDatasourcePoolPreparedStatement;

    @Value("${master.datasource.maxPoolPreparedStatementPerConnectionSize}")
    private int masterDatasourceMaxPoolPreparedStatementPerConnectionSize;

    @Value("${master.datasource.filters}")
    private String masterDatasourceFilter;

    @Value("${master.datasource.connectionProperties}")
    private String masterDatasourceConnectionProperties;

    @Value("${master.datasource.useGlobalDataSourceStat}")
    private boolean masterDatasourceUseGlobalDataSourceStat;

    @Value("${master.datasource.removeAbandoned}")
    private boolean masterDatasourceRemoveAbandoned;

    @Value("${master.datasource.removeAbandonedTimeOut}")
    private int masterDatasourceRemoveAbandonedTimeOut;

    @Bean(name = "mysqlPrimary")
    public DataSource initDataSource() throws SQLException {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(masterDatasourceDriverClass);
        dataSource.setUrl(masterDatasourceUrl);
        dataSource.setUsername(masterDatasourceUserName);
        dataSource.setPassword(masterDatasourcePassword);
        dataSource.setInitialSize(masterDatasourceInitialSize);
        dataSource.setMinIdle(masterDatasourceMinIdle);
        dataSource.setMaxActive(masterDatasourceMaxActive);
        dataSource.setMaxWait(masterDatasourceMaxWait);
        dataSource.setUseUnfairLock(masterDatasourceUseUnfairLock);
        dataSource.setTimeBetweenEvictionRunsMillis(masterDatasourceTimeBetweenEvictionRunsMillis);
        dataSource.setMinEvictableIdleTimeMillis(masterDatasourceMinEvictableIdleTimeMillis);
        dataSource.setValidationQuery(masterDatasourceValidationQuery);
        dataSource.setTestWhileIdle(masterDatasourceTestWhileIdle);
        dataSource.setTestOnBorrow(masterDatasourceTestOnBrowse);
        dataSource.setTestOnReturn(masterDatasourceTestOnReturn);
        dataSource.setPoolPreparedStatements(masterDatasourcePoolPreparedStatement);
        dataSource.setMaxPoolPreparedStatementPerConnectionSize(masterDatasourceMaxPoolPreparedStatementPerConnectionSize);
        dataSource.setFilters(masterDatasourceFilter);
        dataSource.setConnectionProperties(masterDatasourceConnectionProperties);
        dataSource.setUseGlobalDataSourceStat(masterDatasourceUseGlobalDataSourceStat);
        dataSource.setRemoveAbandoned(masterDatasourceRemoveAbandoned);
        dataSource.setRemoveAbandonedTimeout(masterDatasourceRemoveAbandonedTimeOut);
        return dataSource;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager initTransactionManager(@Qualifier("mysqlPrimary") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "globalConfig")
    public GlobalConfig initMybatisGlobalConfig() {
        GlobalConfig globalConfig = new GlobalConfig();
        GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
        dbConfig.setIdType(IdType.UUID);
        globalConfig.setDbConfig(dbConfig);
        return globalConfig;
    }

    @Bean(name = "sqlSessionFactory")
    public SqlSessionFactory initSqlSessionFactory(
            @Qualifier("mysqlPrimary") DataSource dataSource,
            @Qualifier("globalConfig") GlobalConfig globalConfig
    ) throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource);
        sqlSessionFactory.setTypeAliasesPackage("com.maxevo.thumbtack.model.mybatis.mysql");
        sqlSessionFactory.setGlobalConfig(globalConfig);
        OffsetLimitInterceptor offsetLimitInterceptor = new OffsetLimitInterceptor();
        offsetLimitInterceptor.setDialectClass("com.github.miemiedev.mybatis.paginator.dialect.MySQLDialect");
        sqlSessionFactory.setPlugins(new Interceptor[]{
                offsetLimitInterceptor
        });
        try {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            sqlSessionFactory.setMapperLocations(resolver.getResources("mapper/*.xml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sqlSessionFactory.getObject();
    }

}
