package com.maxevo.thumbtack.service.serviceImpl.level;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.PageBoundsMaker;
import com.maxevo.thumbtack.dao.mybatis.mysql.GoalMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.LevelMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.Level;
import com.maxevo.thumbtack.model.mybatis.mysql.LevelExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.level.ILevelService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 10:58
 */
@Service
public class LevelServiceImpl implements ILevelService {

    @Autowired
    private LevelMapper levelMapper;

    @Autowired
    private GoalMapper goalMapper;

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public LevelExt createLevel(LevelExt level) {
        if (StringUtils.isBlank(level.getLevelName())) {
            throw new CustomException(ExceptionCode.LEVEL_PARAM, "等级名不能为空");
        }
        if (StringUtils.isBlank(level.getGoalId())) {
            throw new CustomException(ExceptionCode.LEVEL_PARAM, "等级所属目标 id 不能为空");
        }
        if (null == goalMapper.selectById(level.getGoalId())) {
            throw new CustomException(ExceptionCode.GOAL_NOT_EXIST);
        }
        if (null != levelMapper.selectOne(new QueryWrapper<Level>()
                .eq("goal_id", level.getGoalId())
                .eq("level_name", level.getLevelName()))) {
            throw new CustomException(ExceptionCode.LEVEL_NAME_EXIST);
        }
        level.setLevelOrder(levelMapper.selectCount(new QueryWrapper<Level>()
                .eq("goal_id", level.getGoalId())
        ));
        levelMapper.insert(level);
        return level;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void deleteLevelById(String levelId) {
        Level targetLevel = checkCanBeDeletedById(levelId);
        levelMapper.deleteById(levelId);
        levelMapper.updateOrderAfterDelete(targetLevel.getGoalId(), targetLevel.getLevelOrder());
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void editLevelById(LevelEdit levelEdit) {
        Level targetLevel;
        if (StringUtils.isBlank(levelEdit.getLevelId())) {
            throw new CustomException(ExceptionCode.LEVEL_PARAM, "等级 id 为空");
        }
        if (null == (targetLevel = levelMapper.selectById(levelEdit.getLevelId()))) {
            throw new CustomException(ExceptionCode.LEVEL_NOT_EXIST);
        }
        if (StringUtils.isNotBlank(levelEdit.getLevelName())
                &&
                null != levelMapper.selectOne(new QueryWrapper<Level>()
                        .eq("goal_id", targetLevel.getGoalId())
                        .eq("level_name", levelEdit.getLevelName())
                        .ne("level_id", levelEdit.getLevelId())
                )
        ) {
            throw new CustomException(ExceptionCode.LEVEL_NAME_EXIST);
        }
        levelMapper.updateById(levelEdit.buildLevel());
    }

    @Override
    public PageResponseBody<LevelExt> getLevelsByGoalId(GetLevelsByGoalIdRequestBody param) {
        PageResponseBody<LevelExt> result = null;
        int total;
        if (StringUtils.isBlank(param.getGoalId())) {
            throw new CustomException(ExceptionCode.GOAL_PARAM, "目标 id 为空");
        }
        if (null == goalMapper.selectById(param.getGoalId())) {
            throw new CustomException(ExceptionCode.GOAL_NOT_EXIST);
        }
        if (0 != (total = levelMapper.selectCount(new QueryWrapper<Level>().eq("goal_id", param.getGoalId())))) {
            result = new PageResponseBody<>(
                    levelMapper.getLevelsByGoalId(param.getGoalId(), PageBoundsMaker.make(param)), param, total
            );
        }
        return result;
    }

    @Override
    public void moveLevelById(LevelMove levelMove) {
        Level targetLevel;
        Level upLevel;
        Level downLevel;
        int total;
        if (StringUtils.isBlank(levelMove.getLevelId())) {
            throw new CustomException(ExceptionCode.LEVEL_PARAM, "等级 id 为空");
        }
        if (null == (targetLevel = levelMapper.selectById(levelMove.getLevelId()))) {
            throw new CustomException(ExceptionCode.LEVEL_NOT_EXIST);
        }
        if (null == levelMove.getMoveOperator()) {
            throw new CustomException(ExceptionCode.LEVEL_PARAM, "移动参数为空");
        }
        total = levelMapper.selectCount(new QueryWrapper<Level>().eq("goal_id", targetLevel.getGoalId()));
        upLevel = new Level(false);
        downLevel = new Level(false);
        switch (levelMove.getMoveOperator()) {
            case UP:
                if (0 == targetLevel.getLevelOrder()) {
                    throw new CustomException(ExceptionCode.LEVEL_ORDER_EXCEPTION, "已经是第一个啦");
                }
                upLevel.setLevelId(levelMove.getLevelId());
                upLevel.setLevelOrder(targetLevel.getLevelOrder() - 1);
                downLevel.setLevelOrder(targetLevel.getLevelOrder());
                levelMapper.update(downLevel, new UpdateWrapper<Level>()
                        .eq("goal_id", targetLevel.getGoalId())
                        .eq("level_order", targetLevel.getLevelOrder() - 1)
                );
                levelMapper.updateById(upLevel);
                break;
            case DOWN:
                if (total == targetLevel.getLevelOrder() + 1) {
                    throw new CustomException(ExceptionCode.LEVEL_ORDER_EXCEPTION, "已经是最后一个啦");
                }
                downLevel.setLevelId(levelMove.getLevelId());
                downLevel.setLevelOrder(targetLevel.getLevelOrder() + 1);
                upLevel.setLevelOrder(targetLevel.getLevelOrder());
                levelMapper.update(upLevel, new UpdateWrapper<Level>()
                        .eq("goal_id", targetLevel.getGoalId())
                        .eq("level_order", targetLevel.getLevelOrder() + 1)
                );
                levelMapper.updateById(downLevel);
                break;
            default:
                throw new CustomException(ExceptionCode.LEVEL_PARAM, "移动参数类型无效");
        }

    }

    private Level checkCanBeDeletedById(String levelId) {
        Level result;
        if (StringUtils.isBlank(levelId)) {
            throw new CustomException(ExceptionCode.LEVEL_PARAM, "要删除的等级id不能为空");
        }
        if (null == (result = levelMapper.selectById(levelId))) {
            throw new CustomException(ExceptionCode.LEVEL_NOT_EXIST);
        }
        return result;
    }
}
