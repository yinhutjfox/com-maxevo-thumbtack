package com.maxevo.thumbtack.service.serviceImpl.system;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.maxevo.thumbtack.common.utils.md5.Md5Helper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.dao.mybatis.mysql.UserInfoMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;
import com.maxevo.thumbtack.serviceapi.system.IRegisterAndLoginService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/11 21:37
 */
@Service
public class RegisterAndLoginServiceImpl implements IRegisterAndLoginService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo register(UserInfo userInfo) {
        if (StringUtils.isBlank(userInfo.getUserName())) {
            throw new CustomException(ExceptionCode.USER_INFO_PARAM, "用户名为空");
        }
        if (StringUtils.isBlank(userInfo.getUserAccount())) {
            throw new CustomException(ExceptionCode.USER_INFO_PARAM, "用户账号为空");
        }
        if (StringUtils.isBlank(userInfo.getUserPassword())) {
            throw new CustomException(ExceptionCode.USER_INFO_PARAM, "用户密码为空");
        }
        if (null == userInfo.getRole()) {
            throw new CustomException(ExceptionCode.USER_INFO_PARAM, "用户角色为空");
        }
        if (6 > userInfo.getUserPassword().length() || 20 < userInfo.getUserPassword().length()) {
            throw new CustomException(ExceptionCode.USER_INFO_PARAM, "用户密码因为6~20位");
        }
        if (null != userInfoMapper.selectOne(new QueryWrapper<UserInfo>().eq("user_account", userInfo.getUserAccount()))) {
            throw new CustomException(ExceptionCode.USER_INFO_ACCOUNT_EXIST);
        }
        userInfo.setUserPassword(Md5Helper.getMD5(userInfo.getUserPassword()));
        userInfoMapper.insert(userInfo);
        return userInfo;
    }

    @Override
    public UserInfo login(LoginRequestBody loginRequestBody) {
        UserInfo result;
        if (StringUtils.isBlank(loginRequestBody.getAccount())) {
            throw new CustomException(ExceptionCode.LOGING_PARAM, "账号为空");
        }
        if (StringUtils.isBlank(loginRequestBody.getPassword())) {
            throw new CustomException(ExceptionCode.LOGING_PARAM, "密码为空");
        }
        if (null == (result = userInfoMapper.login(loginRequestBody.getAccount(), Md5Helper.getMD5(loginRequestBody.getPassword())))) {
            throw new CustomException(ExceptionCode.LOGING_EXCEPTION, "账号或密码错误");
        }
        return result;
    }
}
