package com.maxevo.thumbtack.service.serviceImpl.goal;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.PageBoundsMaker;
import com.maxevo.thumbtack.dao.mybatis.mysql.CategoryMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.GoalMapper;
import com.maxevo.thumbtack.dao.mybatis.mysql.LevelMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.Goal;
import com.maxevo.thumbtack.model.mybatis.mysql.GoalExt;
import com.maxevo.thumbtack.model.mybatis.mysql.Level;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import com.maxevo.thumbtack.serviceapi.goal.IGoalService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/12 17:40
 */
@Service
public class GoalServiceImpl implements IGoalService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private GoalMapper goalMapper;

    @Autowired
    private LevelMapper levelMapper;

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public GoalExt createGoal(GoalExt goalExt) {
        if (StringUtils.isBlank(goalExt.getGoalName()) || StringUtils.isBlank(goalExt.getCategoryId())) {
            throw new CustomException(ExceptionCode.GOAL_PARAM, "目标名或所属门类id异常");
        }
        if (null == categoryMapper.selectById(goalExt.getCategoryId())) {
            throw new CustomException(ExceptionCode.CATEGORY_NOT_EXIST);
        }
        if (null != goalMapper.selectOne(new QueryWrapper<Goal>().eq("goal_name", goalExt.getGoalName()).eq("category_id", goalExt.getCategoryId()))) {
            throw new CustomException(ExceptionCode.GOAL_NAME_EXIST);
        }
        goalExt.setGoalOrder(goalMapper.selectCount(new QueryWrapper<Goal>().eq("category_id", goalExt.getCategoryId())));
        goalMapper.insert(goalExt);
        return goalExt;
    }

    @Override
    public PageResponseBody<GoalExt> getGoalsByCategoryId(GetGoalListByCategoryIdRequestBody requestBody) {
        PageResponseBody<GoalExt> result = null;
        int total;
        if (StringUtils.isBlank(requestBody.getCategoryId())) {
            throw new CustomException(ExceptionCode.CATEGORY_PARAM, "所属门类id 为空");
        }
        if (null == categoryMapper.selectById(requestBody.getCategoryId())) {
            throw new CustomException(ExceptionCode.CATEGORY_NOT_EXIST);
        }
        total = goalMapper.selectCount(new QueryWrapper<Goal>()
                .eq("category_id", requestBody.getCategoryId()));
        if (0 != total) {
            result = new PageResponseBody<>(
                    goalMapper.getGoalsByCategoryId(requestBody.getCategoryId(), PageBoundsMaker.make(requestBody)),
                    requestBody,
                    total
            );
        }
        return result;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void deleteGoalById(String goalId) {
        Goal targetGoal = checkCanBeDeletedById(goalId);
        goalMapper.deleteById(goalId);
        goalMapper.updateOrderAfterDelete(targetGoal.getCategoryId(), targetGoal.getGoalOrder());
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void editGoalById(GoalEdit goalEdit) {
        Goal goalEdited;
        if (StringUtils.isBlank(goalEdit.getGoalId())) {
            throw new CustomException(ExceptionCode.GOAL_PARAM, "目标 id 为空");
        }
        if (null == (goalEdited = goalMapper.selectById(goalEdit.getGoalId()))) {
            throw new CustomException(ExceptionCode.GOAL_NOT_EXIST);
        }
        if (StringUtils.isNotBlank(goalEdit.getGoalName())) {
            if (null != goalMapper.selectOne(new QueryWrapper<Goal>().eq("goal_name", goalEdit.getGoalName()).eq("category_id", goalEdited.getCategoryId()).ne("goal_id", goalEdit.getGoalId()))) {
                throw new CustomException(ExceptionCode.GOAL_NAME_EXIST);
            }
        }
        goalMapper.updateById(goalEdit.buildGoal());
    }

    @Override
    public void moveGoalById(GoalMove goalMove) {
        Goal targetGoal;
        Goal upGoal;
        Goal downGoal;
        int total;
        if (StringUtils.isBlank(goalMove.getGoalId())) {
            throw new CustomException(ExceptionCode.GOAL_PARAM, "目标 id 为空");
        }
        if (null == (targetGoal = goalMapper.selectById(goalMove.getGoalId()))) {
            throw new CustomException(ExceptionCode.GOAL_NOT_EXIST);
        }
        if (null == goalMove.getMoveOperator()) {
            throw new CustomException(ExceptionCode.GOAL_PARAM, "移动类型参数为空");
        }
        total = goalMapper.selectCount(new QueryWrapper<Goal>().eq("category_id", targetGoal.getCategoryId()));
        upGoal = new Goal(false);
        downGoal = new Goal(false);
        switch (goalMove.getMoveOperator()) {
            case UP:
                if (0 == targetGoal.getGoalOrder()) {
                    throw new CustomException(ExceptionCode.GOAL_ORDER_EXCEPTION, "已经是第一个啦");
                }
                upGoal.setGoalId(targetGoal.getGoalId());
                upGoal.setGoalOrder(targetGoal.getGoalOrder() - 1);
                downGoal.setGoalOrder(targetGoal.getGoalOrder());
                goalMapper.update(downGoal,
                        new UpdateWrapper<Goal>()
                                .eq("category_id", targetGoal.getCategoryId())
                                .eq("goal_order", targetGoal.getGoalOrder() - 1)
                );
                goalMapper.updateById(upGoal);
                break;
            case DOWN:
                if (total == targetGoal.getGoalOrder() + 1) {
                    throw new CustomException(ExceptionCode.GOAL_ORDER_EXCEPTION, "已经是最后一个啦");
                }
                downGoal.setGoalId(targetGoal.getGoalId());
                downGoal.setGoalOrder(targetGoal.getGoalOrder() + 1);
                upGoal.setGoalOrder(targetGoal.getGoalOrder());
                goalMapper.update(upGoal,
                        new UpdateWrapper<Goal>()
                                .eq("category_id", targetGoal.getCategoryId())
                                .eq("goal_order", targetGoal.getGoalOrder() + 1)
                );
                goalMapper.updateById(downGoal);
                break;
            default:
                throw new CustomException(ExceptionCode.GOAL_PARAM, "移动类型参数为空");
        }
    }

    private Goal checkCanBeDeletedById(String goalId) {
        Goal result;
        if (StringUtils.isBlank(goalId)) {
            throw new CustomException(ExceptionCode.GOAL_PARAM, "目标id 为空");
        }
        if (null == (result = goalMapper.selectById(goalId))) {
            throw new CustomException(ExceptionCode.GOAL_NOT_EXIST);
        }
        if (CollectionUtils.isNotEmpty(levelMapper.selectList(new QueryWrapper<Level>().eq("goal_id", goalId)))) {
            throw new CustomException(ExceptionCode.DELETE_EXCEPTION);
        }
        return result;
    }
}
