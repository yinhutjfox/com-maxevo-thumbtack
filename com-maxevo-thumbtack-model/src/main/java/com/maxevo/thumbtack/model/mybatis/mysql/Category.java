package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/9 12:04
 */
@NoArgsConstructor
@TableName("category")
public class Category implements Serializable {

    private static final long serialVersionUID = 970812076053615489L;

    @ApiParam(hidden = true)
    @TableId(value = "category_id", type = IdType.UUID)
    @ApiModelProperty("门类id")
    @Getter
    @Setter
    private String categoryId;

    @NotEmpty(message = "门类名不能为空")
    @ApiParam(value = "门类名", required = true)
    @ApiModelProperty("门类名")
    @TableField(updateStrategy = FieldStrategy.NOT_EMPTY)
    @Getter
    @Setter
    private String categoryName;

    @ApiParam("门类封面内容")
    @ApiModelProperty("门类封面内容")
    @Getter
    @Setter
    private String categoryCoverContent = "";

    @ApiParam("门类描述")
    @ApiModelProperty("门类描述")
    @Getter
    @Setter
    private String categoryAbstract = "";

    @ApiParam(hidden = true)
    @JsonIgnore
    @Getter
    private Date createTime;

    @ApiParam(hidden = true)
    @JsonIgnore
    @Getter
    private Date updateTime;

    public Category(String categoryId, String categoryName, String categoryAbstract, String categoryCoverContent) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryAbstract = categoryAbstract;
        this.categoryCoverContent = categoryCoverContent;
    }

}
