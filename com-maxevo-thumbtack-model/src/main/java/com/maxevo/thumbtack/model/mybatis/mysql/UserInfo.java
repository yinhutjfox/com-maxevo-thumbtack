package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maxevo.thumbtack.common.utils.role.Role;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/6 22:11
 */
@NoArgsConstructor
@TableName("user_info")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 2017363526938486286L;

    @TableId(value = "user_id", type = IdType.UUID)
    @ApiParam(hidden = true)
    @ApiModelProperty("用户id")
    @Getter
    @Setter
    private String userId;

    @NotEmpty(message = "用户名不能为空")
    @ApiParam(value = "用户名", required = true)
    @ApiModelProperty("用户名")
    @Getter
    @Setter
    private String userName;

    @NotEmpty(message = "用户账户不能为空")
    @ApiParam(value = "用户账户", required = true)
    @ApiModelProperty("用户账户")
    @Getter
    @Setter
    private String userAccount;

    @NotEmpty(message = "用户密码不能为空")
    @ApiParam(value = "用户密码", required = true)
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Getter
    @Setter
    private String userPassword;

    @ApiParam("用户手机")
    @ApiModelProperty(hidden = true)
    @Getter
    @Setter
    private String userPhone;

    @ApiParam("用户邮箱")
    @ApiModelProperty(hidden = true)
    @Getter
    @Setter
    private String userEmail;

    @ApiModelProperty("用户角色")
    @ApiParam(hidden = true)
    @Getter
    @Setter
    private Role role;

    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private Date createTime;

    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private Date updateTime;
}
