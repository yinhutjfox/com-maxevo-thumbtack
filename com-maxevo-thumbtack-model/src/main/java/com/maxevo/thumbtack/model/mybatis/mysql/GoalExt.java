package com.maxevo.thumbtack.model.mybatis.mysql;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/31 22:05
 */
public class GoalExt extends Goal {

    @ApiModelProperty("封面基础路径")
    @ApiParam(hidden = true)
    public String getBasePath() {
        return GlobalProperties.getGOAL_COVER_BASE_PATH();
    }
}
