package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/12 17:21
 */
@NoArgsConstructor
@TableName("goal")
public class Goal implements Serializable {

    private static final long serialVersionUID = -712361239699830399L;

    @ApiModelProperty("目标id")
    @ApiParam(hidden = true)
    @TableId(value = "goal_id", type = IdType.UUID)
    @Getter
    @Setter
    private String goalId;

    @NotEmpty(message = "目标名不能为空")
    @ApiParam(value = "目标名", required = true)
    @ApiModelProperty("目标名")
    @TableField(updateStrategy = FieldStrategy.NOT_EMPTY)
    @Getter
    @Setter
    private String goalName;

    @ApiParam("目标封面内容")
    @ApiModelProperty("目标封面内容")
    @Getter
    @Setter
    private String goalCoverContent = "";

    @ApiParam("目标简介")
    @ApiModelProperty("目标简介")
    @Getter
    @Setter
    private String goalAbstract = "";

    @ApiParam(value = "目标属于的门类id", required = true)
    @ApiModelProperty("目标属于的门类id")
    @Getter
    @Setter
    private String categoryId;

    @ApiParam(hidden = true)
    @ApiModelProperty(value = "目标 排序 从0起", example = "0")
    @Getter
    @Setter
    private Integer goalOrder;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private String createTime;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private String updateTime;

    public Goal(boolean needDefault) {
        if (!needDefault) {
            goalAbstract = null;
            goalCoverContent = null;
        }
    }

}
