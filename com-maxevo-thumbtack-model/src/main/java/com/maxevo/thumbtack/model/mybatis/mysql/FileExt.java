package com.maxevo.thumbtack.model.mybatis.mysql;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 11:46
 */
public class FileExt extends File {

    @ApiModelProperty("文件与服务器对接路径标识")
    @Getter
    private String pathMatch = GlobalProperties.getFILE_PATH_MATCH();

}
