package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 18:11
 */
@NoArgsConstructor
@TableName("subject")
public class Subject implements Serializable {

    private static final long serialVersionUID = 6198461156084013462L;

    @ApiParam(hidden = true)
    @ApiModelProperty("科目id")
    @TableId(value = "subject_id", type = IdType.UUID)
    @Getter
    @Setter
    private String subjectId;

    @ApiParam("科目名")
    @ApiModelProperty("科目名")
    @TableField(updateStrategy = FieldStrategy.NOT_EMPTY)
    @Getter
    @Setter
    private String subjectName;

    @ApiParam("科目封面内容")
    @ApiModelProperty("科目封面内容")
    @Getter
    @Setter
    private String subjectCoverContent = "";

    @ApiParam("科目简介")
    @ApiModelProperty("科目简介")
    @Getter
    @Setter
    private String subjectAbstract = "";

    @JsonIgnore
    private Date createTime;

    @JsonIgnore
    private Date updateTime;
}
