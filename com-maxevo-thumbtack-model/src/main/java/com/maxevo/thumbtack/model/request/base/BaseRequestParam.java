package com.maxevo.thumbtack.model.request.base;

import com.maxevo.thumbtack.common.utils.mybatis.Page;

import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 21:32
 */
public class BaseRequestParam extends Page implements Serializable {

    private static final long serialVersionUID = -8259107163979623471L;
}
