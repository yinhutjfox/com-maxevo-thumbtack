package com.maxevo.thumbtack.model.redis;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.redis.RedisHelper;
import com.maxevo.thumbtack.model.middleware.TokenTag;
import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/18 16:15
 */
public class Token implements Serializable {

    private static final long serialVersionUID = -1746377825401656901L;

    private boolean fromRedis = false;

    @Getter
    @Setter
    private UserInfo userInfo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss:SS", timezone = "GMT+8")
    @Getter
    @Setter
    private Date expireTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss:SS", timezone = "GMT+8")
    @Getter
    @Setter
    private Date createTime = new Date();

    @Getter
    @Setter
    private String token;

    @Getter
    @Setter
    private TokenTag tokenTag;

    /**
     * 持续时长 单位 分钟
     */
    @Getter
    private int durationTime = 15;

    private Token() {
        fromRedis = true;
    }

    public Token(UserInfo userInfo, String token) {
        this.userInfo = userInfo;
        this.token = token;
        updateExpireTime();
    }

    public Token(UserInfo userInfo, String token, TokenTag tokenTag) {
        this.userInfo = userInfo;
        this.token = token;
        this.tokenTag = tokenTag;
        updateExpireTime();
    }

    public void updateExpireTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, durationTime);
        expireTime = calendar.getTime();
        tokenTag.update(token, this);
    }

    public void checkExpired() {
        if (0 < System.currentTimeMillis() - expireTime.getTime()) {
            tokenTag.delete(token);
            throw new CustomException(ExceptionCode.TOKEN_EXPIRE);
        }
    }

    public void setDurationTime(int durationTime) {
        if (0 < durationTime) {
            this.durationTime = durationTime;
            if (!fromRedis) {
                updateExpireTime();
            } else {
                fromRedis = false;
            }
        } else {
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "设置时间必须大于0");
        }
    }

    public void checkValid(String token) {
        if (!this.token.equals(token)) {
            if (StringUtils.isNotBlank(token)) {
                RedisHelper.delete("*" + token);
            }
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "token 无效");
        }
    }

    public void checkTag(TokenTag tokenTag) {
        if (this.tokenTag != tokenTag) {
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "token 类型不匹配");
        }
    }

    public void checkAndUpdate(String tokenStr, TokenTag tokenTag) {
        checkValid(tokenStr);
        checkTag(tokenTag);
        checkExpired();
        updateExpireTime();
    }
}
