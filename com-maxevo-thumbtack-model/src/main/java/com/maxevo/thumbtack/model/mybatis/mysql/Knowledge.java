package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/17 9:57
 */
@NoArgsConstructor
@TableName("knowledge")
public class Knowledge implements Serializable {

    private static final long serialVersionUID = 2346297904250451613L;

    @ApiParam(hidden = true)
    @ApiModelProperty("知识点 id")
    @TableId(value = "knowledge_id", type = IdType.UUID)
    @Getter
    @Setter
    private String knowledgeId;

    @ApiParam(value = "知识点名字", required = true)
    @ApiModelProperty("知识点名字")
    @TableField(updateStrategy = FieldStrategy.NOT_EMPTY)
    @Getter
    @Setter
    private String knowledgeName;

    @ApiParam("知识点封面内容")
    @ApiModelProperty("知识点封面内容")
    @Getter
    @Setter
    private String knowledgeCoverContent = "";

    @ApiParam("知识点简介")
    @ApiModelProperty("知识点简介")
    @Getter
    @Setter
    private String knowledgeAbstract = "";

    @ApiParam("知识点内容")
    @ApiModelProperty("知识点内容")
    @Getter
    @Setter
    private String knowledgeContent = "";

    @ApiParam(hidden = true)
    @ApiModelProperty(value = "知识点排序", example = "0")
    @Getter
    @Setter
    private Integer knowledgeOrder;

    @ApiParam(value = "知识点所属科目 id", required = true)
    @ApiModelProperty("知识点所属科目 id")
    @Getter
    @Setter
    private String subjectId;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private Date createTime;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private Date updateTime;

    public Knowledge(boolean needDefault) {
        if (!needDefault) {
            knowledgeCoverContent = null;
            knowledgeAbstract = null;
            knowledgeContent = null;
        }
    }
}
