package com.maxevo.thumbtack.model.mybatis.mysql;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/8/20 10:07
 */
public class KnowledgeConfigurationExt extends KnowledgeConfiguration {

    @ApiModelProperty("知识点名字")
    @Getter
    @Setter
    private String knowledgeName;

    @ApiModelProperty("知识点封面内容")
    @Getter
    @Setter
    private String knowledgeCoverContent = "";

    @ApiModelProperty("知识点简介")
    @Getter
    @Setter
    private String knowledgeAbstract = "";

    @ApiModelProperty("知识点内容")
    @Getter
    @Setter
    private String knowledgeContent = "";

    @ApiModelProperty(value = "知识点排序", example = "0")
    @Getter
    @Setter
    private Integer knowledgeOrder;

    @ApiModelProperty("科目名")
    @Getter
    @Setter
    private String subjectName;

    @ApiModelProperty("科目封面内容")
    @Getter
    @Setter
    private String subjectCoverContent = "";

    @ApiModelProperty("科目简介")
    @Getter
    @Setter
    private String subjectAbstract = "";

    @ApiModelProperty(value = "科目基础路径")
    public String getSubjectBasePath() {
        return GlobalProperties.getSUBJECT_COVER_BASE_PATH();
    }

    @ApiModelProperty(value = "目标基础路径")
    public String getGoalBasePath() {
        return GlobalProperties.getGOAL_COVER_BASE_PATH();
    }

    @ApiModelProperty(value = "等级基础路径")
    public String getLevelBasePath() {
        return GlobalProperties.getLEVEL_COVER_BASE_PATH();
    }

    @ApiModelProperty(value = "知识点基础路径")
    public String getKnowledgeBasePath() {
        return GlobalProperties.getKNOWLEDGE_COVER_BASE_PATH();
    }
}
