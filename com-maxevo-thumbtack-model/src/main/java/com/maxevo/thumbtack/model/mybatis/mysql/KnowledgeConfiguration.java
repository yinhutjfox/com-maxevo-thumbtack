package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/8/20 10:07
 */
@NoArgsConstructor
@TableName("knowledge_configuration")
public class KnowledgeConfiguration implements Serializable {

    private static final long serialVersionUID = 4532536804986812740L;

    @TableId(value = "knowledge_to_goal_id", type = IdType.INPUT)
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    @JsonIgnore
    @Getter @Setter
    private String knowledgeToGoalId;

    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    @JsonIgnore
    @Getter @Setter
    private String knowledgeToLevelId;

    @ApiParam(value = "知识点id")
    @ApiModelProperty(value = "知识点id")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    @Getter @Setter
    private String knowledgeId;

    @ApiParam(hidden = true)
    @ApiModelProperty(value = "所属目标id")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    @Getter @Setter
    private String goalId;

    @ApiParam(value = "所属等级id")
    @ApiModelProperty(value = "所属等级id")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    @Getter @Setter
    private String levelId;

    @ApiParam(hidden = true)
    @ApiModelProperty(value = "所属门类id")
    @TableField(updateStrategy = FieldStrategy.NOT_NULL)
    @Getter @Setter
    private String subjectId;

    @ApiParam(hidden = true)
    @ApiModelProperty(value = "学习顺序")
    @Getter @Setter
    private int learnOrder;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private Date createTime;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private Date updateTime;

    public KnowledgeConfiguration(String knowledgeId, String goalId, String levelId, String subjectId, int learnOrder) {
        this.knowledgeId = knowledgeId;
        this.goalId = goalId;
        this.levelId = levelId;
        this.subjectId = subjectId;
        this.learnOrder = learnOrder;
        this.knowledgeToGoalId = knowledgeId + goalId;
        this.knowledgeToLevelId = knowledgeId + levelId;
    }

    public KnowledgeConfiguration(Knowledge knowledge, Level level, int learnOrder) {
        this(knowledge.getKnowledgeId(), level.getGoalId(), level.getLevelId(), knowledge.getSubjectId(), learnOrder);
    }

}
