package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 10:42
 */
@NoArgsConstructor
@TableName("level")
public class Level implements Serializable {

    private static final long serialVersionUID = 9102840808448552967L;

    @ApiParam(hidden = true)
    @ApiModelProperty("等级 id")
    @TableId(value = "level_id", type = IdType.UUID)
    @Getter
    @Setter
    private String levelId;

    @NotEmpty(message = "等级名字不能为空")
    @ApiParam("等级名字")
    @ApiModelProperty("等级名字")
    @TableField(updateStrategy = FieldStrategy.NOT_EMPTY)
    @Getter
    @Setter
    private String levelName;

    @ApiParam("等级封面内容")
    @ApiModelProperty("等级封面内容")
    @Getter
    @Setter
    private String levelCoverContent = "";

    @ApiParam("等级简介")
    @ApiModelProperty("等级简介")
    @Getter
    @Setter
    private String levelAbstract = "";

    @ApiParam(hidden = true)
    @ApiModelProperty(value = "等级排序", example = "0")
    @Getter
    @Setter
    private Integer levelOrder;

    @NotEmpty(message = "所属目标 id 不能为空")
    @ApiParam("等级所属 目标 id")
    @ApiModelProperty("等级所属 目标 id")
    @Getter
    @Setter
    private String goalId;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private Date createTime;

    @JsonIgnore
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    private Date updateTime;

    public Level(boolean needDefault) {
        if (!needDefault) {
            levelCoverContent = null;
            levelAbstract = null;
        }
    }

}
