package com.maxevo.thumbtack.model.mybatis.mysql;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maxevo.thumbtack.common.utils.file.FileType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 11:25
 */
@NoArgsConstructor
@TableName("file")
public class File implements Serializable {

    private static final long serialVersionUID = 1499647566698398337L;

    @ApiModelProperty("文件相对路径")
    @TableId(value = "file_relative_path", type = IdType.INPUT)
    @Getter
    @Setter
    private String fileRelativePath;

    @ApiModelProperty("文件类型")
    @Getter
    @Setter
    private FileType fileType;

    @ApiModelProperty("原始文件名")
    @Getter
    @Setter
    private String originName;

    @JsonIgnore
    @Getter
    private Date createTime;

    @JsonIgnore
    @Getter
    private Date updateTime;

}
