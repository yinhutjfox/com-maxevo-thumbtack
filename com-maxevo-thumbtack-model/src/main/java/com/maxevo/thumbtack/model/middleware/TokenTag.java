package com.maxevo.thumbtack.model.middleware;

import com.maxevo.thumbtack.common.utils.json.JSONHelper;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.redis.RedisHelper;
import com.maxevo.thumbtack.common.utils.uuid.UuidHelper;
import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;
import com.maxevo.thumbtack.model.redis.Token;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/18 16:39
 */
public enum TokenTag {

    /**
     * 前台 token
     */
    TOKEN_WEB_API,

    /**
     * 后台 token
     */
    TOKEN_WEB_ADMIN,

    /**
     * 前台 swagger token
     */
    TOKEN_SWAGGER_API,

    /**
     * 后台 swagger token
     */
    TOKEN_SWAGGER_ADMIN,
    ;

    private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SS");

    public Token create(UserInfo userInfo) {
        String key;
        Token token;
        Date createTime = null;
        if (StringUtils.isBlank(key = getTokenByUserId(userInfo.getUserId()))) {
            key = UuidHelper.buildUuid();
        } else {
            if (null != (token = get(key))) {
                try {
                    token.checkAndUpdate(key, this);
                    createTime = token.getCreateTime();
                } catch (Exception error) {
                    key = UuidHelper.buildUuid();
                }
            }
        }
        token = new Token(userInfo, key, this);
        if (null != createTime) {
            token.setCreateTime(createTime);
        }
        RedisHelper.set(toString() + ":" + key, JSONHelper.toJsonString(token));
        RedisHelper.set(toString() + ":" + key + ":" + userInfo.getUserId(), "");
        RedisHelper.set(toString() + ":" + key + ":" + sdf.format(token.getExpireTime()) + ":ExpireTime", "");
        return token;
    }

    public void update(String key, Token token) {
        if (StringUtils.isBlank(key)) {
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "key 不能为空 更新失败");
        }
        RedisHelper.set(toString() + ":" + key, JSONHelper.toJsonString(token));
        RedisHelper.delete(toString() + ":" + key + ":*:ExpireTime");
        RedisHelper.set(toString() + ":" + key + ":" + sdf.format(token.getExpireTime()) + ":ExpireTime", "");
    }

    public Token get(String key) {
        Token result = null;
        Set<String> keys;
        if (StringUtils.isBlank(key)) {
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "key 不能为空 获取失败");
        }
        keys = RedisHelper.keys("*" + key);
        if (CollectionUtils.isNotEmpty(keys)) {
            result = JSONHelper.parseObject(RedisHelper.get(keys.toArray()[0].toString()), Token.class);
            if (null == result) {
                RedisHelper.delete("*" + key);
            }
        }
        return result;
    }

    public void delete(String key) {
        if (StringUtils.isBlank(key)) {
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "key 不能为空 删除失败");
        }
        RedisHelper.delete("*" + key + "*");
    }

    public String getTokenByUserId(String userId) {
        String token = null;
        Set<String> keys;
        if (StringUtils.isBlank(userId)) {
            throw new CustomException(ExceptionCode.TOKEN_EXCEPTION, "用户ID为空");
        }
        keys = RedisHelper.keys(toString() + "*" + userId);
        if (CollectionUtils.isNotEmpty(keys)) {
            String[] tmp = keys.toArray()[0].toString().split(":");
            if (2 > tmp.length) {
                token = null;
            } else {
                token = tmp[tmp.length - 2];
            }
        }

        return token;
    }

    public String getTagName() {
        return toString().replace(":", "-");
    }

    @Override
    public String toString() {
        return super.toString().replace("_", ":");
    }
}
