package com.maxevo.thumbtack.model.mybatis.mysql;

import com.maxevo.thumbtack.common.configuration.GlobalProperties;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/31 22:07
 */
public class LevelExt extends Level {

    @ApiModelProperty("封面基础路径")
    public String getBasePath() {
        return GlobalProperties.getLEVEL_COVER_BASE_PATH();
    }
}
