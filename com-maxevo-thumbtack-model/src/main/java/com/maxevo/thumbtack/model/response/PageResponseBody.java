package com.maxevo.thumbtack.model.response;

import com.maxevo.thumbtack.common.utils.mybatis.Page;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 21:51
 */
public class PageResponseBody<T> implements Serializable {

    private static final long serialVersionUID = -9186888894454206340L;

    @ApiModelProperty("数据内容")
    private List<T> data;

    @ApiModelProperty(value = "总数", example = "0")
    private Integer total = 0;

    @ApiModelProperty(value = "当前页<p style='color:red'>从1起</p>", example = "0")
    private Integer page = 1;

    @ApiModelProperty(value = "该也的行数", example = "0")
    private Integer row = 0;

    private PageResponseBody() {
    }

    public PageResponseBody(List<T> data) {
        if (null != data) {
            setData(data);
        }
    }

    public PageResponseBody(List<T> data, Page page, Integer total) {
        this(data);
        setPage(page);
        setTotal(total);
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
        if (null != data) {
            setRow(data.size());
        }
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setPage(Page page) {
        if (null != page) {
            this.page = page.getPage();
        }
    }

    public Integer getRow() {
        return row;
    }

    private void setRow(Integer row) {
        this.row = row;
    }
}
