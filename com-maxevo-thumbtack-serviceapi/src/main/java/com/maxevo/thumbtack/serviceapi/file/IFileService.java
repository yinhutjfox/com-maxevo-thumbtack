package com.maxevo.thumbtack.serviceapi.file;

import com.maxevo.thumbtack.model.mybatis.mysql.File;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 14:26
 */
public interface IFileService {

    void uploadFile(File file);

}
