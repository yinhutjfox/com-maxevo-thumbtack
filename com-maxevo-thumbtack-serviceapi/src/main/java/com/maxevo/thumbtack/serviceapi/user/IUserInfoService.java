package com.maxevo.thumbtack.serviceapi.user;

import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/8 0:58
 */
public interface IUserInfoService {


    UserInfo getUserInfoById(String userId);

}
