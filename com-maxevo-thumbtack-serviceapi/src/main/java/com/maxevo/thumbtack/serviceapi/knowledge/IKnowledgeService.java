package com.maxevo.thumbtack.serviceapi.knowledge;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.order.MoveOperator;
import com.maxevo.thumbtack.model.mybatis.mysql.Knowledge;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeExt;
import com.maxevo.thumbtack.model.request.base.BaseRequestParam;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/17 12:21
 */
public interface IKnowledgeService {

    KnowledgeExt createKnowledge(KnowledgeExt knowledge);

    void deleteKnowledgeById(String knowledgeId);

    void editKnowledgeById(KnowledgeEdit knowledgeEdit);

    PageResponseBody<KnowledgeExt> getKnowledgeBySubjectId(GetKnowledgeBySubjectIsRequestBody param);

    void moveKnowledgeById(KnowledgeMove knowledgeMove);

    @NoArgsConstructor
    class KnowledgeEdit implements Serializable {

        private static final long serialVersionUID = -8740209321023365297L;

        @NotEmpty(message = "知识点 id 不能为空")
        @ApiParam(value = "要修改的 知识点 id", required = true)
        @Getter
        @Setter
        private String knowledgeId;

        @ApiParam("知识点 名")
        @Getter
        @Setter
        private String knowledgeName;

        @ApiParam("知识点 封面 内容")
        @Getter
        @Setter
        private String knowledgeCoverContent;

        @ApiParam("知识点 简介")
        @Getter
        @Setter
        private String knowledgeAbstract;

        @ApiParam("知识点 内容")
        @Getter
        @Setter
        private String knowledgeContent;

        @JsonIgnore
        public Knowledge buildKnowledge() {
            if (StringUtils.isBlank(knowledgeName) && null == knowledgeCoverContent && null == knowledgeAbstract && null == knowledgeContent) {
                throw new CustomException(ExceptionCode.UPDATE_EXCEPTION);
            }
            Knowledge knowledge = new Knowledge();
            knowledge.setKnowledgeId(knowledgeId);
            knowledge.setKnowledgeName(knowledgeName);
            knowledge.setKnowledgeCoverContent(knowledgeCoverContent);
            knowledge.setKnowledgeAbstract(knowledgeAbstract);
            knowledge.setKnowledgeContent(knowledgeContent);
            return knowledge;
        }
    }

    @NoArgsConstructor
    class GetKnowledgeBySubjectIsRequestBody extends BaseRequestParam {

        @NotEmpty(message = "科目id 不能为空")
        @ApiParam(value = "科目id", required = true)
        @Getter
        @Setter
        private String subjectId;

    }

    @NoArgsConstructor
    class KnowledgeMove implements Serializable {

        private static final long serialVersionUID = -4851574207804601580L;

        @NotEmpty(message = "知识点id不能为空")
        @ApiParam(value = "知识点id", required = true)
        @Getter
        @Setter
        private String knowledgeId;

        @NotNull(message = "操作不能为空")
        @ApiParam(value = "移动操作", required = true)
        @Getter
        @Setter
        private MoveOperator moveOperator;
    }

}
