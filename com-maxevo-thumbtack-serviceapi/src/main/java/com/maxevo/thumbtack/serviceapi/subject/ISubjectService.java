package com.maxevo.thumbtack.serviceapi.subject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.Page;
import com.maxevo.thumbtack.model.mybatis.mysql.Subject;
import com.maxevo.thumbtack.model.mybatis.mysql.SubjectExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 21:11
 */
public interface ISubjectService {

    SubjectExt createSubject(SubjectExt subject);

    void deleteSubjectById(String subjectId);

    void editSubjectById(SubjectEdit subjectEdit);

    PageResponseBody<SubjectExt> getSubjects(Page page);

    @NoArgsConstructor
    class SubjectEdit implements Serializable {

        private static final long serialVersionUID = -5770127792334711674L;

        @NotEmpty(message = "科目id不能为空")
        @ApiParam(value = "科目id", required = true)
        @Getter
        @Setter
        private String subjectId;

        @ApiParam("科目名")
        @Getter
        @Setter
        private String subjectName;

        @ApiParam("科目封面内容")
        @Getter
        @Setter
        private String subjectCoverContent;

        @ApiParam("科目简介")
        @Getter
        @Setter
        private String subjectAbstract;


        @JsonIgnore
        public Subject buildSubject() {
            if (StringUtils.isBlank(subjectName) && null == subjectCoverContent && null == subjectAbstract) {
                throw new CustomException(ExceptionCode.UPDATE_EXCEPTION);
            }
            Subject result = new Subject();
            result.setSubjectId(subjectId);
            result.setSubjectName(subjectName);
            result.setSubjectCoverContent(subjectCoverContent);
            result.setSubjectAbstract(subjectAbstract);
            return result;
        }
    }

}
