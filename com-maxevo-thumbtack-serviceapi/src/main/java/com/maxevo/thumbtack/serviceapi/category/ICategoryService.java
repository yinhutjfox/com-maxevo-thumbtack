package com.maxevo.thumbtack.serviceapi.category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.mybatis.Page;
import com.maxevo.thumbtack.model.mybatis.mysql.Category;
import com.maxevo.thumbtack.model.mybatis.mysql.CategoryExt;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/9 12:15
 */
public interface ICategoryService {

    PageResponseBody<CategoryExt> getCategories(Page page);

    String createCategory(Category category);

    void editCategoryById(CategoryEdit categoryEdit);

    void deleteCategoryById(String categoryId);

    @NoArgsConstructor
    class CategoryEdit implements Serializable {

        private static final long serialVersionUID = -5930000292065929087L;

        @NotEmpty(message = "要修改的category 的 id 不能为空")
        @ApiParam(value = "要修改的category 的 id", required = true)
        @Getter
        @Setter
        private String categoryId;

        @ApiParam("新的名字")
        @Getter
        @Setter
        private String newCategoryName;

        @ApiParam("新的简介")
        @Getter
        @Setter
        private String newCategoryAbstract;

        @ApiParam("新的封面")
        @Getter
        @Setter
        private String newCategoryCoverContent;

        @JsonIgnore
        public Category buildCategory() {
            if (StringUtils.isBlank(newCategoryName) && null == newCategoryAbstract && null == newCategoryCoverContent) {
                throw new CustomException(ExceptionCode.UPDATE_EXCEPTION);
            }
            return new Category(categoryId, newCategoryName, newCategoryAbstract, newCategoryCoverContent);
        }
    }

}
