package com.maxevo.thumbtack.serviceapi.knowledge;

import com.maxevo.thumbtack.common.utils.order.MoveOperator;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeConfigurationExt;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeExt;
import com.maxevo.thumbtack.model.request.base.BaseRequestParam;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/8/20 10:42
 */
public interface IKnowledgeConfigurationService {

    /**
     * 配置知识点到等级上
     *
     * @param param 配置知识点的参数，见{@link ConfigKnowledgeRequestBody}
     * @return 返回配置进去的知识点的排序
     */
    int configKnowledge(ConfigKnowledgeRequestBody param);

    /**
     * 将知识点从配置中移除并维护排序
     * @param param 配置知识点的参数，见{@link RemoveKnowledgeFromLevelRequestBody}
     * @return 返回移除的知识点在科目中的排序
     */
    int removeKnowledgeFromLevel(RemoveKnowledgeFromLevelRequestBody param);

    /**
     * 调整知识点配置的排序
     *
     * @param param 知识点排序的参数，见{@link MoveKnowledgeByIdRequestBody}
     */
    void moveKnowledgeById(MoveKnowledgeByIdRequestBody param);

    /**
     * 通过等级id获取配置的知识点
     *
     * @param param 包含等级id及分页参数
     * @return 返回等级下的知识点配置数据
     */
    PageResponseBody<KnowledgeConfigurationExt> getKnowledgeConfigurationsByLevelId(GetKnowledgeConfigurationsByLevelIdRequestBody param);

    /**
     * 获取等级下相关科目可以配置进去的知识点
     *
     * @param param 包含等级id、科目id及分页参数
     * @return 返回等级下相关科目可以配置进去的知识点
     */
    PageResponseBody<KnowledgeExt> getKnowledgeCanBeUsed(GetKnowledgeCanBeUsedRequestBody param);



    @NoArgsConstructor
    class ConfigKnowledgeRequestBody implements Serializable {

        private static final long serialVersionUID = -8880553550070287726L;

        @ApiParam(value = "知识点id")
        @NotBlank(message = "知识点id不能为空")
        @Getter @Setter
        private String knowledgeId;

        @ApiParam(value = "等级id")
        @NotBlank(message = "等级id不能为空")
        @Getter @Setter
        private String levelId;
    }

    @NoArgsConstructor
    class RemoveKnowledgeFromLevelRequestBody implements Serializable {
        private static final long serialVersionUID = -2503723801381165935L;

        @ApiParam(value = "知识点id")
        @NotBlank(message = "知识点id不能为空")
        @Getter @Setter
        private String knowledgeId;

        @ApiParam(value = "等级id")
        @NotBlank(message = "等级id不能为空")
        @Getter @Setter
        private String levelId;

    }

    @NoArgsConstructor
    class GetKnowledgeConfigurationsByLevelIdRequestBody extends BaseRequestParam {

        @NotBlank(message = "等级id不能为空")
        @ApiParam(value = "等级id")
        @Getter @Setter
        private String levelId;

    }

    @NoArgsConstructor
    class GetKnowledgeCanBeUsedRequestBody extends BaseRequestParam {

        @NotBlank(message = "等级id不能为空")
        @ApiParam(value = "等级id")
        @Getter @Setter
        private String levelId;

        @NotBlank(message = "科目id不能为空")
        @ApiParam(value = "科目id")
        @Getter @Setter
        private String subjectId;

    }

    @NoArgsConstructor
    class MoveKnowledgeByIdRequestBody implements Serializable {

        private static final long serialVersionUID = 7379230922987405087L;

        @ApiParam(value = "知识点id")
        @NotBlank(message = "知识点id不能为空")
        @Getter @Setter
        private String knowledgeId;

        @ApiParam(value = "等级id")
        @NotBlank(message = "等级id不能为空")
        @Getter @Setter
        private String levelId;

        @NotNull(message = "操作类型不能为空")
        @ApiParam(value = "移动的类型<p style='color:red'>UP-上移</p><p style='color:green'>DOWN-下移</p>", required = true)
        @Getter
        @Setter
        private MoveOperator moveOperator;
    }


}
