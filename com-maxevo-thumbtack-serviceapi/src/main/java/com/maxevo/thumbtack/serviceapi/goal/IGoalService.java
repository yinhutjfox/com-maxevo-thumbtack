package com.maxevo.thumbtack.serviceapi.goal;

import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.order.MoveOperator;
import com.maxevo.thumbtack.model.mybatis.mysql.Goal;
import com.maxevo.thumbtack.model.mybatis.mysql.GoalExt;
import com.maxevo.thumbtack.model.request.base.BaseRequestParam;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/12 17:39
 */
public interface IGoalService {

    GoalExt createGoal(GoalExt goalExt);

    void deleteGoalById(String goalId);

    PageResponseBody<GoalExt> getGoalsByCategoryId(GetGoalListByCategoryIdRequestBody requestBody);

    void editGoalById(GoalEdit goalEdit);

    void moveGoalById(GoalMove goalMove);

    @NoArgsConstructor
    class GetGoalListByCategoryIdRequestBody extends BaseRequestParam {

        @NotEmpty(message = "所属门类id 不能为空")
        @ApiParam(value = "所属门类id", required = true)
        @Getter
        @Setter
        private String categoryId;

    }

    @NoArgsConstructor
    class GoalEdit implements Serializable {

        private static final long serialVersionUID = 5328202846599100802L;

        @NotEmpty(message = "目标id不能为空")
        @ApiParam(value = "目标 id", required = true)
        @Getter
        @Setter
        private String goalId;

        @ApiParam(value = "目标名")
        @Getter
        @Setter
        private String goalName;

        @ApiParam("目标封面内容")
        @Getter
        @Setter
        private String goalCoverContent;

        @ApiParam("目标简介")
        @Getter
        @Setter
        private String goalAbstract;

        public Goal buildGoal() {
            if (StringUtils.isBlank(goalName) && null == goalCoverContent && null == goalAbstract) {
                throw new CustomException(ExceptionCode.UPDATE_EXCEPTION);
            }
            Goal goal = new Goal();
            goal.setGoalId(goalId);
            goal.setGoalName(goalName);
            goal.setGoalCoverContent(goalCoverContent);
            goal.setGoalAbstract(goalAbstract);
            return goal;
        }
    }

    @NoArgsConstructor
    class GoalMove implements Serializable {

        private static final long serialVersionUID = -4079615390863992229L;

        @NotEmpty(message = "目标 id 不能为空")
        @ApiParam(value = "移动的goalId", required = true)
        @Getter
        @Setter
        private String goalId;

        @NotNull(message = "操作类型不能为空")
        @ApiParam(value = "移动的类型<p style='color:red'>UP-上移</p><p style='color:green'>DOWN-下移</p>", required = true)
        @Getter
        @Setter
        private MoveOperator moveOperator;
    }

}
