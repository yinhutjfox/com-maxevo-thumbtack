package com.maxevo.thumbtack.serviceapi.system;

import com.maxevo.thumbtack.model.middleware.TokenTag;
import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;
import com.maxevo.thumbtack.model.redis.Token;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/11 21:03
 */
public interface IRegisterAndLoginService {

    UserInfo register(UserInfo userInfo);

    UserInfo login(LoginRequestBody loginRequestBody);

    @NoArgsConstructor
    class LoginResponseBody implements Serializable {

        private static final long serialVersionUID = 8729774296498411250L;

        @ApiParam("用户信息")
        @Getter
        @Setter
        private UserInfo userInfo;

        @ApiParam("token 标识")
        @Setter
        private TokenTag tokenTag;

        @ApiParam("token")
        @Getter
        @Setter
        private String token;

        public LoginResponseBody(UserInfo userInfo, TokenTag tokenTag, String token) {
            this.userInfo = userInfo;
            this.tokenTag = tokenTag;
            this.token = token;
        }

        public LoginResponseBody(Token token) {
            this.userInfo = token.getUserInfo();
            this.tokenTag = token.getTokenTag();
            this.token = token.getToken();
        }

        public String getTokenTag() {
            return tokenTag.getTagName();
        }
    }

    @NoArgsConstructor
    class SwaggerLoginResponseBody extends LoginResponseBody {

        @ApiModelProperty("swagger 跳转地址")
        @Getter
        @Setter
        private String swaggerURL;

        public SwaggerLoginResponseBody(UserInfo userInfo, TokenTag tokenTag, String token, String swaggerURL) {
            super(userInfo, tokenTag, token);
            this.swaggerURL = swaggerURL;
        }

        public SwaggerLoginResponseBody(Token token, String swaggerURL) {
            super(token);
            this.swaggerURL = swaggerURL;
        }

    }

    @NoArgsConstructor
    class LoginRequestBody implements Serializable {

        private static final long serialVersionUID = -2243640578993414441L;

        @NotEmpty(message = "账号不能为空")
        @ApiParam(value = "账号", required = true)
        @Getter
        @Setter
        private String account;

        @NotEmpty(message = "密码不能为空")
        @ApiParam(value = "密码", required = true)
        @Getter
        @Setter
        private String password;
    }

}
