package com.maxevo.thumbtack.serviceapi.level;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maxevo.thumbtack.common.utils.messages.CustomException;
import com.maxevo.thumbtack.common.utils.messages.ExceptionCode;
import com.maxevo.thumbtack.common.utils.order.MoveOperator;
import com.maxevo.thumbtack.model.mybatis.mysql.Level;
import com.maxevo.thumbtack.model.mybatis.mysql.LevelExt;
import com.maxevo.thumbtack.model.request.base.BaseRequestParam;
import com.maxevo.thumbtack.model.response.PageResponseBody;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 10:57
 */
public interface ILevelService {

    LevelExt createLevel(LevelExt level);

    void deleteLevelById(String levelId);

    void editLevelById(LevelEdit levelEdit);

    PageResponseBody<LevelExt> getLevelsByGoalId(GetLevelsByGoalIdRequestBody param);

    void moveLevelById(LevelMove levelMove);

    @NoArgsConstructor
    class GetLevelsByGoalIdRequestBody extends BaseRequestParam {

        @NotEmpty(message = "所属 目标 id 不能为空")
        @ApiParam(value = "所属 目标 id", required = true)
        @Getter
        @Setter
        private String goalId;

    }

    @NoArgsConstructor
    class LevelEdit implements Serializable {

        private static final long serialVersionUID = 7601042851357836048L;

        @NotEmpty(message = "等级id不能为空")
        @ApiParam("要修改的等级id")
        @Getter
        @Setter
        private String levelId;

        @ApiParam("等级名字")
        @Getter
        @Setter
        private String levelName;

        @ApiParam("等级封面")
        @Getter
        @Setter
        private String levelCoverContent;

        @ApiParam("等级简介")
        @Getter
        @Setter
        private String levelAbstract;

        @JsonIgnore
        public Level buildLevel() {
            if (StringUtils.isBlank(levelName) && null == levelCoverContent && null == levelAbstract) {
                throw new CustomException(ExceptionCode.UPDATE_EXCEPTION);
            }
            Level level = new Level();
            level.setLevelId(levelId);
            level.setLevelName(levelName);
            level.setLevelCoverContent(levelCoverContent);
            level.setLevelAbstract(levelAbstract);
            return level;
        }

    }

    @NoArgsConstructor
    class LevelMove implements Serializable {

        private static final long serialVersionUID = -9198471220232534740L;

        @NotEmpty(message = "等级 id 不能为空")
        @ApiParam(required = true)
        @Getter
        @Setter
        private String levelId;

        @NotNull(message = "移动操作不能为空")
        @ApiParam(required = true)
        @Getter
        @Setter
        private MoveOperator moveOperator;
    }

}
