package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.maxevo.thumbtack.model.mybatis.mysql.Level;
import com.maxevo.thumbtack.model.mybatis.mysql.LevelExt;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 10:56
 */
public interface LevelMapper extends BaseMapper<Level> {

    List<LevelExt> getLevelsByGoalId(@Param("goalId") String goalId, PageBounds pageBounds);

    int updateOrderAfterDelete(@Param("goalId") String goalId, @Param("deletedOrder") int deletedOrder);
}
