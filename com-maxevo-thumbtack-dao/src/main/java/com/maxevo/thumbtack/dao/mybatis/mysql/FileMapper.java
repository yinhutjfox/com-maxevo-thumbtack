package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.File;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/10 11:48
 */
public interface FileMapper extends BaseMapper<File> {
}
