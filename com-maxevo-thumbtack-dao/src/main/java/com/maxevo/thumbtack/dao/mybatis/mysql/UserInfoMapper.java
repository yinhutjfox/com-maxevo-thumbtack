package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.maxevo.thumbtack.model.mybatis.mysql.UserInfo;
import org.apache.ibatis.annotations.Param;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/6 22:17
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    UserInfo login(@Param("account") String account, @Param("password") String password);

}
