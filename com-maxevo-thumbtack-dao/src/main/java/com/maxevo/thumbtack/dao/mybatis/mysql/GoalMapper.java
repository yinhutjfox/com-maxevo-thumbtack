package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.maxevo.thumbtack.model.mybatis.mysql.Goal;
import com.maxevo.thumbtack.model.mybatis.mysql.GoalExt;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/12 17:34
 */
public interface GoalMapper extends BaseMapper<Goal> {

    List<GoalExt> getGoalsByCategoryId(@Param("categoryId") String categoryId, PageBounds pageBounds);

    int updateOrderAfterDelete(@Param("categoryId") String categoryId, @Param("deletedOrder") int deletedOrder);

}
