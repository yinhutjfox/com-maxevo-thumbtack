package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.maxevo.thumbtack.model.mybatis.mysql.Knowledge;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeExt;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/17 10:27
 */
public interface KnowledgeMapper extends BaseMapper<Knowledge> {

    /**
     * 在删除后根据删除知识点所在的科目id与当前排序更新排序
     *
     * @param subjectId 删除的知识点所在的科目id
     * @param deletedOrder 删除的知识点的排序
     * @return 影响行数
     */
    int updateOrderAfterDeleteById(@Param("subjectId") String subjectId, @Param("deletedOrder") int deletedOrder);

    /**
     * 获取一个科目下的知识点
     *
     * @param subjectId 科目id
     * @param pageBounds 分页参数
     * @return 扩展知识点实体
     */
    List<KnowledgeExt> getKnowledgeBySubjectId(@Param("subjectId") String subjectId, PageBounds pageBounds);

    /**
     * 获取一个等级下某个科目可用于配置的知识点数目<br/>
     * - 这里之所以用目标id来过滤是因为设计的时候考虑一个知识点在一个目标下只能出现一次。同时也就间接达成一个知识点在一个等级下只
     * 出现一次，因为等级从属于目标
     *
     * @param goalId 目标id(等级所属目标id)
     * @param subjectId 科目id
     * @return 等级下某个科目可用于配置的知识点数目
     */
    int getKnowledgeCanBeUsedCount(@Param("goalId") String goalId, @Param("subjectId") String subjectId);

    /**
     * 获取一个等级下某个科目可用于配置的知识点<br/>
     * - 这里之所以用目标id来过滤是因为设计的时候考虑一个知识点在一个目标下只能出现一次。同时也就间接达成一个知识点在一个等级下只
     * 出现一次，因为等级从属于目标
     *
     * @param goalId 目标id(等级所属目标id)
     * @param subjectId 科目id
     * @param pageBounds 分页参数
     * @return 等级下某个科目可用于配置的知识点
     */
    List<KnowledgeExt> getKnowledgeCanBeUsed(@Param("goalId") String goalId, @Param("subjectId") String subjectId, PageBounds pageBounds);

}
