package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeConfiguration;
import com.maxevo.thumbtack.model.mybatis.mysql.KnowledgeConfigurationExt;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/8/20 10:41
 */
public interface KnowledgeConfigurationMapper extends BaseMapper<KnowledgeConfiguration> {

    /**
     * 根据等级id查找配置进去的知识点并排序
     *
     * @param levelId 等级id
     * @param pageBounds 分页参数
     * @return 扩展配置实体 {@link KnowledgeConfigurationExt}
     */
    List<KnowledgeConfigurationExt> getKnowledgeConfigurationsByLevelId(@Param("levelId") String levelId, PageBounds pageBounds);

    /**
     * 在移除配置后维护排序
     *
     * @param levelId 等级id
     * @param removedOrder 移除的排序
     * @return
     */
    int updateOrderAfterRemove(@Param("levelId") String levelId, @Param("removedOrder") int removedOrder);

}
