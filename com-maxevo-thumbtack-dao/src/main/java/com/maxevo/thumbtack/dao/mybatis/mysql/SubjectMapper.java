package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.maxevo.thumbtack.model.mybatis.mysql.Subject;
import com.maxevo.thumbtack.model.mybatis.mysql.SubjectExt;

import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/16 21:00
 */
public interface SubjectMapper extends BaseMapper<Subject> {

    List<SubjectExt> getSubjects(PageBounds pageBounds);
}
