package com.maxevo.thumbtack.dao.mybatis.mysql;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.maxevo.thumbtack.model.mybatis.mysql.Category;
import com.maxevo.thumbtack.model.mybatis.mysql.CategoryExt;

import java.util.List;

/**
 * @author JingFeng.Tan
 * @version 1.0.0
 * @date 2019/7/9 12:10
 */
public interface CategoryMapper extends BaseMapper<Category> {

    List<CategoryExt> getCategories(PageBounds pageBounds);
}
